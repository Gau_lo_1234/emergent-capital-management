<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Email
| -------------------------------------------------------------------------
| This file lets you define parameters for sending emails.
| Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/libraries/email.html
|
*/



$config['mailtype']     = 'html';
$config['useragent']    = 'Post Title';
$config['protocol']     = 'smtp';
$config['smtp_host']    = '';
$config['smtp_user']    = '';
$config['smtp_pass']    = '';
$config['smtp_port']    = '587';
//$config['charset']      = 'UTF-8';
$config['smtp_timeout'] = '300';
$config['wordwrap']     = TRUE;
$config['validation'] = FALSE;
$config['newline']      = "\r\n";  



/* End of file email.php */
/* Location: ./application/config/email.php */