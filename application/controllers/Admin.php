<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    require APPPATH . '/libraries/BaseController.php';
/*  
 *  @author     : Gaudencio Solivatore
 *  date        : 27 september, 2014
 *  Android Device Management Prp
 *  
 *  gaulomail@gmail.com
 */

class Admin extends BaseController
{
    
    var $array_data;
    var $header;

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->library('Excel');
        $this->load->library('ciqrcode');
        $this->load->model('m_data_captured');
        $this->load->model('upload_diamond_model');
    
    
       /*cache control*/
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
     
        
    }
    
    /***default functin, redirects to login page if no admin logged in yet***/
    public function index()
    {
     
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url() . 'homepage', 'refresh');
        if ($this->session->userdata('admin_login') == 1)
            redirect(base_url() . 'dashboard', 'refresh');
    }
    
    /***ADMIN DASHBOARD***/
    function dashboard()
    {
     
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');
        $page_data['page_name']  = 'dashboard';
        $page_data['users_online']  =$this->m_data_captured->get_online_users();
        $page_data['page_title'] = get_phrase('admin_dashboard');
        $page_data['role']=$this->session->userdata('role_id');
        $this->load->view('backend/index', $page_data);
    }
    
   
    
    
/****MANAGE DISTRIBUTORS*****/
function distributor($param1 = '', $param2 = '', $param3 = '')
{
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'create') {
        $data['distributor_name']        = $this->input->post('distributor_name');
        $data['distributor_web']    = $this->input->post('distributor_web');
        $data['distributor_address']     = $this->input->post('distributor_address');
        $data['distributor_phone']       = $this->input->post('distributor_phone');
        $data['email']       = $this->input->post('email');
        $this->db->insert('distributor', $data);

        $distributor_id = $this->db->insert_id();
        $dater['server_ip']=$this->input->post('server_ip');
        $dater['server_port']=$this->input->post('server_port');
        $dater['server_name']=$this->input->post('distributor_name');
        $dater['distributor_id']=$this->server_id();
        $this->db->insert('server_settings', $dater);
        move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/distributor_image/' . $distributor_id . '.jpg');
        $this->session->set_flashdata('flash_message' , get_phrase('data_added_successfully'));
        //$this->email_model->account_opening_email('distributor', $data['distributor_email']); //SEND EMAIL ACCOUNT OPENING EMAIL
        redirect(base_url() . 'distributor', 'refresh');
    }
    if ($param1 == 'do_update') {
        $data['distributor_name']        = $this->input->post('distributor_name');
        $data['distributor_web']    = $this->input->post('distributor_web');
        $data['distributor_address']     = $this->input->post('distributor_address');
        $data['distributor_phone']       = $this->input->post('distributor_phone');
        $data['email']       = $this->input->post('email');
        
        $this->db->where('distributor_id', $param2);
        $this->db->update('distributor', $data);
        move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/distributor_image/' . $param2 . '.jpg');
        $this->session->set_flashdata('flash_message' , get_phrase('data_updated'));
        redirect(base_url() . 'distributor', 'refresh');
    } else if ($param1 == 'personal_profile') {
        $page_data['personal_profile']   = true;
        $page_data['current_distributor_id'] = $param2;
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('distributor', array(
            'distributor_id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('distributor_id', $param2);
        $data['isDeleted'] = '1';
        $this->db->update('distributor', $data);
        $this->session->set_flashdata('flash_message' , get_phrase('data_deleted'));
        redirect(base_url() . 'distributor', 'refresh');
    }
    $page_data['distributor']   = $this->db->get('distributor')->result_array();
    $page_data['role']=$this->session->userdata('role_id');
    $page_data['page_name']  = 'distributor';
    $page_data['page_title'] = "Manage Distributor";//get_phrase('manage_teacher');
    $this->load->view('backend/index', $page_data);
}

/****MANAGE FAMILY LIST*****/
function familylist($param1 = '', $param2 = '', $param3 = '')
{
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'create') {
        $data['member_name']        = $this->input->post('member_name');
        $data['member_phone']    = $this->input->post('member_phone');
        $data['member_email']     = $this->input->post('member_email');
        $data['family_code']       = $this->input->post('family_code');
        $data['member_code']       = $this->input->post('member_code');
        $data['created']       = time();
        $data['modified']       = time();
        $this->db->insert('FamilyList', $data);
        $familylist_id = $this->db->insert_id();

        move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/family_image/'. $familylist_id .'.jpg');
        $this->session->set_flashdata('flash_message' , get_phrase('data_added_successfully'));
        //$this->email_model->account_opening_email('distributor', $data['distributor_email']); //SEND EMAIL ACCOUNT OPENING EMAIL
        redirect(base_url() . 'familylist', 'refresh');
    }
    if ($param1 == 'do_update') {
        $data['member_name']        = $this->input->post('member_name');
        $data['member_phone']    = $this->input->post('member_phone');
        $data['member_email']     = $this->input->post('member_email');
        $data['family_code']       = $this->input->post('family_code');
        $data['member_code']       = $this->input->post('member_code');
        $data['modified']       = time();
        $this->db->where('member_id', $param2);
        $this->db->update('FamilyList', $data);
        move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/family_image/' .$param2 .'.jpg');
        $this->session->set_flashdata('flash_message' , get_phrase('data_updated'));
        redirect(base_url() . 'familylist', 'refresh');
    } else if ($param1 == 'personal_profile') {
        $page_data['personal_profile']   = true;
        $page_data['current_familylist_id'] = $param2;
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('FamilyList', array(
            'member_id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('member_id', $param2);
        $data['isDeleted'] = '1';
        $data['modified']       = time();
        $this->db->update('FamilyList', $data);
        $this->session->set_flashdata('flash_message' , get_phrase('data_deleted'));
        redirect(base_url() . 'familylist', 'refresh');
    }
    $page_data['familylist']   = $this->db->get('FamilyList')->result_array();
    $page_data['role']=$this->session->userdata('role_id');
    $page_data['page_name']  = 'familylist';
    $page_data['page_title'] = "Manage Family List";//get_phrase('manage_teacher');
    $this->load->view('backend/index', $page_data);
}

/****MANAGE LEARNER LIST*****/
function learnerlist($param1 = '', $param2 = '', $param3 = '')
{
    if ($param1=='upload') {
        $this->load->library('Excel');
        try {
            move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/student_bulk_add.xlsx');
            $file_type	= PHPExcel_IOFactory::identify(UPLOAD_PATH."student_bulk_add.xlsx");
            $objReader	= PHPExcel_IOFactory::createReader($file_type);
           
           try{
               $objPHPExcel = $objReader->load(UPLOAD_PATH."student_bulk_add.xlsx");
           }catch(Exception $e){ $e->getMessage();}
            //$objPHPExcel= $objReader->load(base_url()."uploads/student_bulk_add.xlsx");
          
            $sheet_data	= $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
            foreach($sheet_data as $data)
            {
                $result = array(
                        'internal_id' => $data['A'],
                        'learner_name' => $data['B'],
                        'learner_surname' => $data['C'],
                        'admission_number' => $data['D'],
                        'id_number' => $data['E'],
                        'learner_email' => $data['F'],
                        'learner_cellphone' => $data['G'],
                        'learner_grade' => $data['H'],
                        'register_class' => $data['I'],
                        'gender' => $data['J'],
                        'nationality' => $data['K'],
                        'preferred_language' => $data['L'],
                        'tuition_language' => $data['Y'],
                        'learner_status' => $data['M'],
                        'family_code' => $data['N'],
                        'parent_id' => $data['Z'],
                        'parent_title' => $data['AA'],
                        'parent_name' => $data['AB'],
                        'parent_id_number' => $data['AD'],
                        'parent_email' => $data['AG'],
                        'parent_cellphone' => $data['AF'],
                        'parent_relationship' => $data['AG'],
                        'parent_gender' => $data['AH'],
                        'parent_nationality' => $data['AI'],
                        'parent_language' => $data['AJ'],
                        'created' => time()
                     
                );
               //print_r($result);
               // exit()m
                $this->upload_diamond_model->postDiamond($result);
            }
        } catch (Exception $e) {
            print_r($e->getMessage());
        }

       
        
       
      
      /* 
        $file= UPLOAD_PATH."student_bulk_add.xlsx";//base_url()."uploads/student_bulk_add.xlsx";
        $obj=PHPExcel_IOFactory::load($file);
        
        $cell=$obj->getActiveSheet()->getCellCollection();
   
        foreach ($cell as $c1) {
            $column=$obj->getActiveSheet()->getCell($c1)->getColumn();
            $row=$obj->getActiveSheet()->getCell($c1)->getRow();
            $data_value=$obj->getActiveSheet()->getCell($c1)->getValue();
       
            if ($row==1) {
                $header[$row][$column]=$data_value;
            } else {
                $arr_data[$row][$column]=$data_value;
            }
        }
    */
        redirect(base_url() . 'learnerlist');
    }
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'create') {
        $data['internal_id']        = $this->input->post('internal_id');
        $data['learner_name']        = $this->input->post('learner_name');
        $data['learner_surname']        = $this->input->post('learner_surname');
        $data['parent_cellphone']    = $this->input->post('parent_cellphone');
        $data['learner_email']     = $this->input->post('learner_email');
        $data['parent_email']     = $this->input->post('parent_email');
        $data['family_code']       = $this->input->post('family_code');
        $data['created']       = time();
        $data['modified']       = time();
        $this->db->insert('LearnerList', $data);
        $familylist_id = $this->db->insert_id();

        move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/family_image/'. $familylist_id .'.jpg');
        $this->session->set_flashdata('flash_message' , get_phrase('data_added_successfully'));
        //$this->email_model->account_opening_email('distributor', $data['distributor_email']); //SEND EMAIL ACCOUNT OPENING EMAIL
        redirect(base_url() . 'learnerlist', 'refresh');
    }
    if ($param1 == 'do_update') {
        $data['internal_id']        = $this->input->post('internal_id');
        $data['learner_name']        = $this->input->post('learner_name');
        $data['learner_surname']        = $this->input->post('learner_surname');
        $data['parent_cellphone']    = $this->input->post('parent_cellphone');
        $data['learner_email']     = $this->input->post('learner_email');
        $data['parent_email']     = $this->input->post('parent_email');
        $data['family_code']       = $this->input->post('family_code');
        $data['modified']       = time();
        $this->db->where('learner_id', $param2);
        $this->db->update('LearnerList', $data);
        move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/family_image/' .$param2 .'.jpg');
        $this->session->set_flashdata('flash_message' , get_phrase('data_updated'));
        redirect(base_url() . 'learnerlist', 'refresh');
    } else if ($param1 == 'personal_profile') {
        $page_data['personal_profile']   = true;
        $page_data['current_learnerlist_id'] = $param2;
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('FamilyList', array(
            'member_id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('learner_id', $param2);
        $data['isDeleted'] = '1';
        $data['modified']       = time();
        $this->db->update('LearnerList', $data);
        $this->session->set_flashdata('flash_message' , get_phrase('data_deleted'));
        redirect(base_url() . 'learnerlist', 'refresh');
    }
    $page_data['learnerlist']   = $this->db->get('LearnerList')->result_array();
    $page_data['role']=$this->session->userdata('role_id');
    $page_data['page_name']  = 'learnerlist';
    $page_data['page_title'] = "Manage Learner List";//get_phrase('manage_teacher');
    $page_data['header']=$header;
    $page_data['values']=$arr_data;
    $this->load->view('backend/index', $page_data);
}
/****MANAGE LEARNER LIST*****/

/****MANAGE PARENT LIST*****/
function parentlist($param1 = '', $param2 = '', $param3 = '')
{
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'create') {
        $data['internal_id']        = $this->input->post('internal_id');
        $data['learner_name']        = $this->input->post('learner_name');
        $data['learner_surname']        = $this->input->post('learner_surname');
        $data['parent_cellphone']    = $this->input->post('parent_cellphone');
        $data['learner_email']     = $this->input->post('learner_email');
        $data['parent_email']     = $this->input->post('parent_email');
        $data['family_code']       = $this->input->post('family_code');
        $data['created']       = time();
        $data['modified']       = time();
        $this->db->insert('LearnerList', $data);
        $familylist_id = $this->db->insert_id();

        move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/family_image/'. $familylist_id .'.jpg');
        $this->session->set_flashdata('flash_message' , get_phrase('data_added_successfully'));
        //$this->email_model->account_opening_email('distributor', $data['distributor_email']); //SEND EMAIL ACCOUNT OPENING EMAIL
        redirect(base_url() . 'parentlist', 'refresh');
    }
    if ($param1 == 'do_update') {
        $data['internal_id']        = $this->input->post('internal_id');
        $data['learner_name']        = $this->input->post('learner_name');
        $data['learner_surname']        = $this->input->post('learner_surname');
        $data['parent_cellphone']    = $this->input->post('parent_cellphone');
        $data['learner_email']     = $this->input->post('learner_email');
        $data['parent_email']     = $this->input->post('parent_email');
        $data['family_code']       = $this->input->post('family_code');
        $data['modified']       = time();
        $this->db->where('learner_id', $param2);
        $this->db->update('LearnerList', $data);
        move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/family_image/' .$param2 .'.jpg');
        $this->session->set_flashdata('flash_message' , get_phrase('data_updated'));
        redirect(base_url() . 'parentlist', 'refresh');
    } else if ($param1 == 'personal_profile') {
        $page_data['personal_profile']   = true;
        $page_data['current_learnerlist_id'] = $param2;
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('LearnerList', array(
            'member_id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('learner_id', $param2);
        $data['isDeleted'] = '1';
        $data['modified']       = time();
        $this->db->update('LearnerList', $data);
        $this->session->set_flashdata('flash_message' , get_phrase('data_deleted'));
        redirect(base_url() . 'parentlist', 'refresh');
    }
    $page_data['learnerlist']   = $this->db->get('LearnerList')->result_array();
    $page_data['role']=$this->session->userdata('role_id');
    $page_data['page_name']  = 'parentlist';
    $page_data['page_title'] = "Manage parent List";//get_phrase('manage_teacher');
    $this->load->view('backend/index', $page_data);
}
/****MANAGE LEARNER LIST*****/

    /****MANAGE USERS*****/
    function users($param1 = '', $param2 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');
        if ($param1 == 'create') {
            $password="admin";
            $data['name']         = $this->input->post('name');
            $data['email'] = $this->input->post('email');
            $data['level']   = $this->input->post('level');
            $data['phone']   = $this->input->post('phone');
            $data['distributor_id']   = $this->input->post('distributor_id');
            $data['password']=getHashedPassword($password);
            $data['password_set']=$this->password_setup();
            $this->db->insert('users', $data);
            $user_id = $this->db->insert_id();
            move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/users_image/' . $user_id . '.jpg');
            $this->email_model->account_opening_email('users', $data['email'], $data['password_set']); //SEND EMAIL ACCOUNT OPENING EMAIL
            $this->session->set_flashdata('flash_message' , get_phrase('data_added_successfully'));
           
            redirect(base_url() . 'users', 'refresh');
           
        }
        if ($param1 == 'do_update') {
            $password="admin";
            $data['name']         = $this->input->post('name');
            $data['email'] = $this->input->post('email');
            $data['level']   = $this->input->post('level');
            $data['phone']   = $this->input->post('phone');
            $data['distributor_id']   = $this->input->post('distributor_id');
            $data['password']=getHashedPassword($password);
            $this->db->where('user_id', $param2);
            $this->db->update('users', $data);
            move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/users_image/' . $param2 . '.jpg');
            $this->session->set_flashdata('flash_message' , get_phrase('data_updated'));
            redirect(base_url() . 'users', 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('users', array(
                'user_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
           
            $this->db->where('user_id', $param2);
            $data['isDeleted'] = '1';
            $this->db->update('users', $data);
            $this->session->set_flashdata('flash_message' , get_phrase('data_deleted'));
            redirect(base_url() . 'users', 'refresh');
        }
        $page_data['users']    = $this->db->get('users')->result_array();
        $page_data['page_name']  = 'users';
        $page_data['role']=$this->session->userdata('role_id');
        $page_data['page_title'] = "manage users";//get_phrase('manage_class');
        $this->load->view('backend/index', $page_data);
    }
/***UNIQUE CODE GENERATOR */
function device_id(){
    $starter_id=167772161;
    $this->db->select('*');
    $this->db->from('devices');
    $this->db->order_by("id", "desc");
    $this->db->limit(1);
    $query = $this->db->get();
    if($query->num_rows()>0){
        foreach ($query->result() as $row)
        {
           $new_value=$row->id;
           $starter_id=  $starter_id + $new_value;
          
        }
    } 
    $device_id= strtoupper(dechex ($starter_id));
    return $device_id;
}


/***UNIQUE CODE GENERATOR */


/***ADD ALERT MESSAGE*/
function add_alert(){
    $data['device_id']= $this->input->post('device_id');
    $data['message']    = $this->input->post('message');
    $data['date']    = time();
    $this->db->insert('alert_messages', $data);
}
/***ADD ALERT MESSAGE*/

/***IMPORT FUNCTION*/
function import($param=''){
   if($param='save'){
        
    if ($this->input->post('importfile')) {
        //$path = ROOT_UPLOAD_IMPORT_PATH;
        $path = base_url()."uploads/";
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'xlsx|xls|jpg|png';
        $config['remove_spaces'] = TRUE;
        $this->upload->initialize($config);
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors());
        } else {
            $data = array('upload_data' => $this->upload->data());
        }
        
        if (!empty($data['upload_data']['file_name'])) {
            $import_xls_file = $data['upload_data']['file_name'];
        } else {
            $import_xls_file = 0;
        }
        $inputFileName = $path . $import_xls_file;
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                    . '": ' . $e->getMessage());
        }
        $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
        
        $arrayCount = count($allDataInSheet);
        $flag = 0;
        $createArray = array('First_Name', 'Last_Name', 'Email', 'DOB', 'Contact_NO');
        $makeArray = array('First_Name' => 'First_Name', 'Last_Name' => 'Last_Name', 'Email' => 'Email', 'DOB' => 'DOB', 'Contact_NO' => 'Contact_NO');
        $SheetDataKey = array();
        foreach ($allDataInSheet as $dataInSheet) {
            foreach ($dataInSheet as $key => $value) {
                if (in_array(trim($value), $createArray)) {
                    $value = preg_replace('/\s+/', '', $value);
                    $SheetDataKey[trim($value)] = $key;
                } else {
                    
                }
            }
        }
        $data = array_diff_key($makeArray, $SheetDataKey);
       
        if (empty($data)) {
            $flag = 1;
        }
        if ($flag == 1) {
            for ($i = 2; $i <= $arrayCount; $i++) {
                $addresses = array();
                $firstName = $SheetDataKey['First_Name'];
                $lastName = $SheetDataKey['Last_Name'];
                $email = $SheetDataKey['Email'];
                $dob = $SheetDataKey['DOB'];
                $contactNo = $SheetDataKey['Contact_NO'];
                $firstName = filter_var(trim($allDataInSheet[$i][$firstName]), FILTER_SANITIZE_STRING);
                $lastName = filter_var(trim($allDataInSheet[$i][$lastName]), FILTER_SANITIZE_STRING);
                $email = filter_var(trim($allDataInSheet[$i][$email]), FILTER_SANITIZE_EMAIL);
                $dob = filter_var(trim($allDataInSheet[$i][$dob]), FILTER_SANITIZE_STRING);
                $contactNo = filter_var(trim($allDataInSheet[$i][$contactNo]), FILTER_SANITIZE_STRING);
                $fetchData[] = array('first_name' => $firstName, 'last_name' => $lastName, 'email' => $email, 'dob' => $dob, 'contact_no' => $contactNo);
            }              
            $data['employeeInfo'] = $fetchData;
            $this->import->setBatchImport($fetchData);
            $this->import->importData();
        } else {
            echo "Please import correct file";
        }
    }
    $this->load->view('import/display', $data);
   }
}
/***IMPORT FUNCTION*/


/***ADD CURRENT LOCATION*/
function add_gps_location(){
    $data['device_id']  = $this->input->post('device_id');
    $data['long']    = $this->input->post('long');
    $data['lati']    = $this->input->post('lati');
    $data['date']    = time();
    $this->db->insert('gps_location', $data);
}
/***ADD CURRENT LOCATION*/


/***SHOW CURRENT LOCATION*/
function locate($imei=''){
   $this->db->where('device_id', $imei);
   $this->db->limit(1);
   $this->db->order_by("date", "desc");
   $query=$this->db->get("gps_location");
   if($query->num_rows()>0){
      // echo '<iframe style="width:100%; height:100%" frameBorder="0" src="http://maps.google.com/maps?q=-26.0810224,27.9218617&z=15&output=embed"></iframe>
        //';
        foreach ($query->result() as $row)
{
    echo '<iframe style="width:100%; height:100%" frameBorder="0" src="http://maps.google.com/maps?q='.$row->lati.','.$row->long.'&z=15&output=embed"></iframe>
        '; 
}
       
   }
    else{
        echo "<h1 align=center>Last location not known</h1>";
    }

  
}
/***SHOW CURRENT LOCATION*/


function email_sent($param=''){
    echo $param;
    echo "gaulo";
}

/***LAST SERVER SETTING GENERATOR */
function server_id(){
    $starter_id=0;
    $this->db->select('*');
    $this->db->from('distributor');
    $this->db->order_by("distributor_id", "desc");
    $this->db->limit(1);
    $query = $this->db->get();
    if($query->num_rows()>0){
        foreach ($query->result() as $row)
        {
           $new_value=$row->distributor_id;
           $starter_id=  $new_value ;
          
        }
    } 
  
    return $starter_id;
}
/***LAST SERVER SETTING GENERATOR */

/****MANAGE DEVICES*****/
function devices($param1 = '', $param2 = '')
{
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
        if($param1=='bulk_add'){
            $file_info = pathinfo($_FILES["result_file"]["name"]);
            $file_directory = "uploads/";
            $new_file_name = date("d-m-Y ") . rand(000000, 999999) .".". $file_info["extension"];
            
            if(move_uploaded_file($_FILES["result_file"]["tmp_name"], $file_directory . $new_file_name))
            {   
                $file_type	= PHPExcel_IOFactory::identify($file_directory . $new_file_name);
                $objReader	= PHPExcel_IOFactory::createReader($file_type);
               
               $objPHPExcel = $objReader->load(UPLOAD_PATH. $new_file_name);
                //$objPHPExcel= $objReader->load(base_url()."uploads/student_import.xlsx");
                $sheet_data	= $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
               
                foreach($sheet_data as $data)
                {
                    $result = array(
                            'name' => $data['A'],
                            
                    );
                    $this->Uploaddiamond_Model->postDiamond($result);
                }
            }
        }
    if ($param1 == 'create') {
        $image=$this->input->post('qrcode');;
        $data['ptt']         = $this->input->post('ptt');
        $data['sms']         = $this->input->post('sms');
        $data['armed']         = $this->input->post('armed');
        $data['advanced_alert']         = $this->input->post('advanced');
        $data['control_room']         = $this->input->post('control');
        $username=$data['username']         = $this->input->post('username');
        $data['phone'] = $this->input->post('phone');
        $email= $data['email']   = $this->input->post('email');
        $data['device_id'] =  $device_id  = $this->input->post('device_id');
        $data['encrypt']=md5($device_id);
        $data['distributor_id']   = $this->input->post('distributor_id');
        $this->db->insert('devices', $data);
        $this->email_model->device_register_email($username,$image, $email); 
        $this->session->set_flashdata('flash_message' , get_phrase('data_added_successfully'));
        redirect(base_url() . 'devices', 'refresh');
    }
    if ($param1 == 'do_update') {
        $data['ptt']         = $this->input->post('ptt');
        $data['sms']         = $this->input->post('sms');
        $data['armed']         = $this->input->post('armed');
        $data['advanced_alert']         = $this->input->post('advanced');
        $data['control_room']         = $this->input->post('control');
        $data['username']         = $this->input->post('username');
        $data['phone'] = $this->input->post('phone');
        $data['email']   = $this->input->post('email');
       
       
        $this->db->where('id', $param2);
        $this->db->update('devices', $data);
        $this->session->set_flashdata('flash_message' , get_phrase('data_updated'));
        redirect(base_url() . 'devices', 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('devices', array(
            'id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('id', $param2);
        $data['isDeleted'] = '1';
        $this->db->update('devices', $data);;
        $this->session->set_flashdata('flash_message' , get_phrase('data_deleted'));
        redirect(base_url() . 'devices', 'refresh');
    }
   
    $page_data['role']=$this->session->userdata('role_id');
    $page_data['page_name']  = 'devices';
    $page_data['device_id']  = $this->device_id();
    $page_data['page_title'] = "manage devices";//get_phrase('manage_class');
    $params['data'] = md5($this->device_id());
    $params['level'] = 'H';
    $params['size'] = 5;
    $params['savename'] = FCPATH.'uploads/qrcodes_image/'. $this->device_id().'.jpg';
    $this->ciqrcode->generate($params);
    $page_data['qrcode'] ='<img src="'.base_url().'uploads/qrcodes_image/'. $this->device_id().'.jpg" />';
    $this->load->view('backend/index', $page_data);
}
/****MANAGE DEVICES*****/
function all_emails_sent($param1 = '', $param2 = '')
{
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'create') {
        $image=$this->input->post('qrcode');;
        $data['ptt']         = $this->input->post('ptt');
        $data['sms']         = $this->input->post('sms');
        $data['armed']         = $this->input->post('armed');
        $data['advanced_alert']         = $this->input->post('advanced');
        $data['control_room']         = $this->input->post('control');
        $username=$data['username']         = $this->input->post('username');
        $data['phone'] = $this->input->post('phone');
        $data['email']   = $this->input->post('email');
        $data['device_id'] = $email= $device_id  = $this->input->post('device_id');
        $data['encrypt']=md5($device_id);
        $data['distributor_id']   = $this->input->post('distributor_id');
        $this->db->insert('devices', $data);
        $this->email_model->device_register_email($username,$image, $email); 
        $this->session->set_flashdata('flash_message' , get_phrase('data_added_successfully'));
        redirect(base_url() . 'devices', 'refresh');
    }
    if ($param1 == 'do_update') {
        $data['ptt']         = $this->input->post('ptt');
        $data['sms']         = $this->input->post('sms');
        $data['armed']         = $this->input->post('armed');
        $data['advanced_alert']         = $this->input->post('advanced');
        $data['control_room']         = $this->input->post('control');
        $data['username']         = $this->input->post('username');
        $data['phone'] = $this->input->post('phone');
        $data['email']   = $this->input->post('email');
       
       
        $this->db->where('id', $param2);
        $this->db->update('devices', $data);
        $this->session->set_flashdata('flash_message' , get_phrase('data_updated'));
        redirect(base_url() . 'devices', 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('devices', array(
            'id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('id', $param2);
        $data['isDeleted'] = '1';
        $this->db->update('devices', $data);;
        $this->session->set_flashdata('flash_message' , get_phrase('data_deleted'));
        redirect(base_url() . 'devices', 'refresh');
    }
   
    $page_data['role']=$this->session->userdata('role_id');
    $page_data['page_name']  = 'all_emails_sent';
    $page_data['device_id']  = $this->device_id();
    $page_data['page_title'] = "view sent emails";//get_phrase('manage_class');
    $params['data'] = md5($this->device_id());
    $params['level'] = 'H';
    $params['size'] = 5;
    $params['savename'] = FCPATH.'uploads/qrcodes_image/'. $this->device_id().'.jpg';
    $this->ciqrcode->generate($params);
    $page_data['qrcode'] ='<img src="'.base_url().'uploads/qrcodes_image/'. $this->device_id().'.jpg" />';
    $this->load->view('backend/index', $page_data);
}
function account_activate(){
    $password=$this->input->post('password');
    $password_set=$this->input->post('password_set');
    
    $this->email_model->account_activate($password, $password_set); 
   // header("Location:".base_url()."index.php?login/account_activate");
}
function forgot_password(){
    $email=$this->input->post('email');
    $password_set=$this->password_setup();
    $this->email_model->password_reset_email($email, $password_set); 
   
    
}

function send_email(){
       
    //$this->load->library('email');
   
    //Email config test settings tes
    $config['mailtype']     = 'html';
    $config['useragent']    = 'Post Title';
    $config['protocol']     = 'smtp';
    $config['smtp_host']    = 'smtp.magtouch.co.za';
    $config['smtp_user']    = 'gautest@magtouch.co.za';
    $config['smtp_pass']    = 'SBtuGPxA6Gye';
    $config['smtp_port']    = '587';
   // $config['charset']      = 'UTF-8';
    $config['smtp_timeout'] = '300';
    $config['wordwrap']     = TRUE;
    $config['validation'] = FALSE;
    $config['newline']      = "\r\n";  

    //send mail
    $this->email->initialize($config);
    $this->email->from('gautest@magtouch.co.za', 'Gaudencio Solivatore');
    $this->email->to('gaulomail@gmail.com');
    $this->email->cc('gautest@magtouch.co.za');
    //$this->email->bcc('gaulomail@gmail.com');
    $this->email->subject('Email Test');
    $this->email->message('Testing the email class.');
    
    //send message
    if($this->email->send()){
        echo "email sent";
        
    }
    else 
    
    echo $this->email->print_debugger();
   
}

/****MANAGE server_settings*****/
function server_settings($param1 = '', $param2 = '')
{
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'create') {
        $dist_id=$data['distributor_id']   = $this->input->post('distributor_id');
        $data['server_ip']         = $this->input->post('server_ip');
        $data['server_port'] = $this->input->post('server_port');
        $data['server_name']   = $this->crud_model->get_distributor_name_by_id( $dist_id);
       
        $this->db->insert('server_settings', $data);
        $this->session->set_flashdata('flash_message' , get_phrase('data_added_successfully'));
        redirect(base_url() . 'server_settings', 'refresh');
    }
    if ($param1 == 'do_update') {
        $data['server_ip']         = $this->input->post('server_ip');
        $data['server_port'] = $this->input->post('server_port');
        $this->db->where('id', $param2);
        $this->db->update('server_settings', $data);
        $this->session->set_flashdata('flash_message' , get_phrase('data_updated'));
        redirect(base_url() . 'server_settings', 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('server_settings', array(
            'id' => $param2
        ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('id', $param2);
        $data['isDeleted'] = '1';
        $this->db->update('server_settings', $data);;
        $this->session->set_flashdata('flash_message' , get_phrase('data_deleted'));
        redirect(base_url() . 'server_settings', 'refresh');
    }
   
    $page_data['role']=$this->session->userdata('role_id');
    $page_data['page_name']  = 'server_settings';
    $page_data['device_id']  = $this->device_id();
    $page_data['page_title'] = get_phrase('manage servers');
    $this->load->view('backend/index', $page_data);
}


    /****MANAGE SECTIONS*****/
    function section($class_id = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');
        // detect the first class
        if ($class_id == '')
            $class_id           =   $this->db->get('class')->first_row()->class_id;

        $page_data['page_name']  = 'section';
        $page_data['page_title'] = get_phrase('manage_sections');
        $page_data['class_id']   = $class_id;
        $this->load->view('backend/index', $page_data);    
    }

    function sections($param1 = '' , $param2 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');
        if ($param1 == 'create') {
            $data['name']       =   $this->input->post('name');
            $data['nick_name']  =   $this->input->post('nick_name');
            $data['class_id']   =   $this->input->post('class_id');
            $data['teacher_id'] =   $this->input->post('teacher_id');
            $this->db->insert('section' , $data);
            $this->session->set_flashdata('flash_message' , get_phrase('data_added_successfully'));
            redirect(base_url() . 'section' . $data['class_id'] , 'refresh');
        }

        if ($param1 == 'edit') {
            $data['name']       =   $this->input->post('name');
            $data['nick_name']  =   $this->input->post('nick_name');
            $data['class_id']   =   $this->input->post('class_id');
            $data['teacher_id'] =   $this->input->post('teacher_id');
            $this->db->where('section_id' , $param2);
            $this->db->update('section' , $data);
            $this->session->set_flashdata('flash_message' , get_phrase('data_updated'));
            redirect(base_url() . 'section' . $data['class_id'] , 'refresh');
        }

        if ($param1 == 'delete') {
            $this->db->where('section_id' , $param2);
            $this->db->delete('section');
            $this->session->set_flashdata('flash_message' , get_phrase('data_deleted'));
            redirect(base_url() . 'section' , 'refresh');
        }
    }

    function get_class_section($class_id)
    {
        $sections = $this->db->get_where('section' , array(
            'class_id' => $class_id
        ))->result_array();
        foreach ($sections as $row) {
            echo '<option value="' . $row['section_id'] . '">' . $row['name'] . '</option>';
        }
    }

    /***MANAGE EVENT / NOTICEBOARD, WILL BE SEEN BY ALL ACCOUNTS DASHBOARD**/
    function noticeboard($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');
        
        if ($param1 == 'create') {
            $data['notice_title']     = $this->input->post('notice_title');
            $data['notice']           = $this->input->post('notice');
            $data['create_timestamp'] = strtotime($this->input->post('create_timestamp'));
            $this->db->insert('noticeboard', $data);

            $check_sms_send = $this->input->post('check_sms');

            if ($check_sms_send == 1) {
                // sms sending configurations

                $parents  = $this->db->get('parent')->result_array();
                $students = $this->db->get('student')->result_array();
                $teachers = $this->db->get('teacher')->result_array();
                $date     = $this->input->post('create_timestamp');
                $message  = $data['notice_title'] . ' ';
                $message .= get_phrase('on') . ' ' . $date;
                foreach($parents as $row) {
                    $reciever_phone = $row['phone'];
                    $this->sms_model->send_sms($message , $reciever_phone);
                }
                foreach($students as $row) {
                    $reciever_phone = $row['phone'];
                    $this->sms_model->send_sms($message , $reciever_phone);
                }
                foreach($teachers as $row) {
                    $reciever_phone = $row['phone'];
                    $this->sms_model->send_sms($message , $reciever_phone);
                }
            }

            $this->session->set_flashdata('flash_message' , get_phrase('data_added_successfully'));
            redirect(base_url() . 'noticeboard', 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['notice_title']     = $this->input->post('notice_title');
            $data['notice']           = $this->input->post('notice');
            $data['create_timestamp'] = strtotime($this->input->post('create_timestamp'));
            $this->db->where('notice_id', $param2);
            $this->db->update('noticeboard', $data);

            $check_sms_send = $this->input->post('check_sms');

            if ($check_sms_send == 1) {
                // sms sending configurations

                $parents  = $this->db->get('parent')->result_array();
                $students = $this->db->get('student')->result_array();
                $teachers = $this->db->get('teacher')->result_array();
                $date     = $this->input->post('create_timestamp');
                $message  = $data['notice_title'] . ' ';
                $message .= get_phrase('on') . ' ' . $date;
                foreach($parents as $row) {
                    $reciever_phone = $row['phone'];
                    $this->sms_model->send_sms($message , $reciever_phone);
                }
                foreach($students as $row) {
                    $reciever_phone = $row['phone'];
                    $this->sms_model->send_sms($message , $reciever_phone);
                }
                foreach($teachers as $row) {
                    $reciever_phone = $row['phone'];
                    $this->sms_model->send_sms($message , $reciever_phone);
                }
            }

            $this->session->set_flashdata('flash_message' , get_phrase('data_updated'));
            redirect(base_url() . 'noticeboard', 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('noticeboard', array(
                'notice_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('notice_id', $param2);
            $this->db->delete('noticeboard');
            $this->session->set_flashdata('flash_message' , get_phrase('data_deleted'));
            redirect(base_url() . 'noticeboard', 'refresh');
        }
        $page_data['page_name']  = 'noticeboard';
        $page_data['role']=$this->session->userdata('role_id');
        $page_data['page_title'] = get_phrase('manage_noticeboard');
        $page_data['notices']    = $this->db->get('noticeboard')->result_array();
        $this->load->view('backend/index', $page_data);
    }
    
    /* private messaging */

    function message($param1 = 'message_home', $param2 = '', $param3 = '') {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        if ($param1 == 'send_new') {
            $message_thread_code = $this->crud_model->send_new_private_message();
            $this->session->set_flashdata('flash_message', get_phrase('message_sent!'));
            redirect(base_url() . 'message/message_read/' . $message_thread_code, 'refresh');
        }

        if ($param1 == 'send_reply') {
            $this->crud_model->send_reply_message($param2);  //$param2 = message_thread_code
            $this->session->set_flashdata('flash_message', get_phrase('message_sent!'));
            redirect(base_url() . 'message/message_read/' . $param2, 'refresh');
        }

        if ($param1 == 'message_read') {
            $page_data['current_message_thread_code'] = $param2;  // $param2 = message_thread_code
            $this->crud_model->mark_thread_messages_read($param2);
        }

        $page_data['message_inner_page_name']   = $param1;
        $page_data['role']=$this->session->userdata('role_id');
        $page_data['page_name']                 = 'message';
        $page_data['page_title']                = get_phrase('private_messaging');
        $this->load->view('backend/index', $page_data);
    }
    
    /*****SITE/SYSTEM SETTINGS*********/
    function system_settings($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url() . 'index.php?login', 'refresh');
        
        if ($param1 == 'do_update') {
             
            $data['description'] = $this->input->post('system_name');
            $this->db->where('type' , 'system_name');
            $this->db->update('settings' , $data);

            $data['description'] = $this->input->post('system_title');
            $this->db->where('type' , 'system_title');
            $this->db->update('settings' , $data);

            $data['description'] = $this->input->post('address');
            $this->db->where('type' , 'address');
            $this->db->update('settings' , $data);

            $data['description'] = $this->input->post('phone');
            $this->db->where('type' , 'phone');
            $this->db->update('settings' , $data);

            $data['description'] = $this->input->post('paypal_email');
            $this->db->where('type' , 'paypal_email');
            $this->db->update('settings' , $data);

            $data['description'] = $this->input->post('currency');
            $this->db->where('type' , 'currency');
            $this->db->update('settings' , $data);

            $data['description'] = $this->input->post('system_email');
            $this->db->where('type' , 'system_email');
            $this->db->update('settings' , $data);

            $data['description'] = $this->input->post('system_name');
            $this->db->where('type' , 'system_name');
            $this->db->update('settings' , $data);

            $data['description'] = $this->input->post('language');
            $this->db->where('type' , 'language');
            $this->db->update('settings' , $data);

            $data['description'] = $this->input->post('text_align');
            $this->db->where('type' , 'text_align');
            $this->db->update('settings' , $data);
            
            $this->session->set_flashdata('flash_message' , get_phrase('data_updated')); 
            redirect(base_url() . 'system_settings', 'refresh');
        }
        if ($param1 == 'upload_logo') {
            move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/logo.png');
            $this->session->set_flashdata('flash_message', get_phrase('settings_updated'));
            redirect(base_url() . 'system_settings', 'refresh');
        }
        if ($param1 == 'change_skin') {
            $data['description'] = $param2;
            $this->db->where('type' , 'skin_colour');
            $this->db->update('settings' , $data);
            $this->session->set_flashdata('flash_message' , get_phrase('theme_selected')); 
            redirect(base_url() . 'system_settings/', 'refresh'); 
        }
        $page_data['page_name']  = 'system_settings';
        $page_data['role']=$this->session->userdata('role_id');
        $page_data['page_title'] = get_phrase('system_settings');
        $page_data['settings']   = $this->db->get('settings')->result_array();
        $this->load->view('backend/index', $page_data);
    }
    
    /***** UPDATE PRODUCT *****/
    
    function update( $task = '', $purchase_code = '' ) {
        
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');
            
        // Create update directory.
        $dir    = 'update';
        if ( !is_dir($dir) )
            mkdir($dir, 0777, true);
        
        $zipped_file_name   = $_FILES["file_name"]["name"];
        $path               = 'update/' . $zipped_file_name;
        
        move_uploaded_file($_FILES["file_name"]["tmp_name"], $path);
        
        // Unzip uploaded update file and remove zip file.
        $zip = new ZipArchive;
        $res = $zip->open($path);
        if ($res === TRUE) {
            $zip->extractTo('update');
            $zip->close();
            unlink($path);
        }
        
        $unzipped_file_name = substr($zipped_file_name, 0, -4);
        $str                = file_get_contents('./update/' . $unzipped_file_name . '/update_config.json');
        $json               = json_decode($str, true);
        

            
        // Run php modifications
        require './update/' . $unzipped_file_name . '/update_script.php';
        
        // Create new directories.
        if(!empty($json['directory'])) {
            foreach($json['directory'] as $directory) {
                if ( !is_dir( $directory['name']) )
                    mkdir( $directory['name'], 0777, true );
            }
        }
        
        // Create/Replace new files.
        if(!empty($json['files'])) {
            foreach($json['files'] as $file)
                copy($file['root_directory'], $file['update_directory']);
        }
        
        $this->session->set_flashdata('flash_message' , get_phrase('product_updated_successfully'));
        redirect(base_url() . 'system_settings');
    }

    /*****SMS SETTINGS*********/
    function sms_settings($param1 = '' , $param2 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url() . 'index.php?login', 'refresh');
        if ($param1 == 'clickatell') {

            $data['description'] = $this->input->post('clickatell_user');
            $this->db->where('type' , 'clickatell_user');
            $this->db->update('settings' , $data);

            $data['description'] = $this->input->post('clickatell_password');
            $this->db->where('type' , 'clickatell_password');
            $this->db->update('settings' , $data);

            $data['description'] = $this->input->post('clickatell_api_id');
            $this->db->where('type' , 'clickatell_api_id');
            $this->db->update('settings' , $data);

            $this->session->set_flashdata('flash_message' , get_phrase('data_updated'));
            redirect(base_url() . 'sms_settings/', 'refresh');
        }

        if ($param1 == 'twilio') {

            $data['description'] = $this->input->post('twilio_account_sid');
            $this->db->where('type' , 'twilio_account_sid');
            $this->db->update('settings' , $data);

            $data['description'] = $this->input->post('twilio_auth_token');
            $this->db->where('type' , 'twilio_auth_token');
            $this->db->update('settings' , $data);

            $data['description'] = $this->input->post('twilio_sender_phone_number');
            $this->db->where('type' , 'twilio_sender_phone_number');
            $this->db->update('settings' , $data);

            $this->session->set_flashdata('flash_message' , get_phrase('data_updated'));
            redirect(base_url() . 'sms_settings', 'refresh');
        }

        if ($param1 == 'active_service') {

            $data['description'] = $this->input->post('active_sms_service');
            $this->db->where('type' , 'active_sms_service');
            $this->db->update('settings' , $data);

            $this->session->set_flashdata('flash_message' , get_phrase('data_updated'));
            redirect(base_url() . 'sms_settings', 'refresh');
        }

        $page_data['page_name']  = 'sms_settings';
        $page_data['page_title'] = get_phrase('sms_settings');
        $page_data['role']=$this->session->userdata('role_id');
        $page_data['settings']   = $this->db->get('settings')->result_array();
        $this->load->view('backend/index', $page_data);
    }
    
    /*****LANGUAGE SETTINGS*********/
    function manage_language($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url() . 'index.php?login', 'refresh');
        
        if ($param1 == 'edit_phrase') {
            $page_data['edit_profile']  = $param2;  
        }
        if ($param1 == 'update_phrase') {
            $language   =   $param2;
            $total_phrase   =   $this->input->post('total_phrase');
            for($i = 1 ; $i < $total_phrase ; $i++)
            {
                //$data[$language]  =   $this->input->post('phrase').$i;
                $this->db->where('phrase_id' , $i);
                $this->db->update('language' , array($language => $this->input->post('phrase'.$i)));
            }
            redirect(base_url() . 'manage_language/edit_phrase/'.$language, 'refresh');
        }
        if ($param1 == 'do_update') {
            $language        = $this->input->post('language');
            $data[$language] = $this->input->post('phrase');
            $this->db->where('phrase_id', $param2);
            $this->db->update('language', $data);
            $this->session->set_flashdata('flash_message', get_phrase('settings_updated'));
            redirect(base_url() . 'manage_language', 'refresh');
        }
        if ($param1 == 'add_phrase') {
            $data['phrase'] = $this->input->post('phrase');
            $this->db->insert('language', $data);
            $this->session->set_flashdata('flash_message', get_phrase('settings_updated'));
            redirect(base_url() . 'manage_language', 'refresh');
        }
        if ($param1 == 'add_language') {
            $language = $this->input->post('language');
            $this->load->dbforge();
            $fields = array(
                $language => array(
                    'type' => 'LONGTEXT'
                )
            );
            $this->dbforge->add_column('language', $fields);
            
            $this->session->set_flashdata('flash_message', get_phrase('settings_updated'));
            redirect(base_url() . 'manage_language', 'refresh');
        }
        if ($param1 == 'delete_language') {
            $language = $param2;
            $this->load->dbforge();
            $this->dbforge->drop_column('language', $language);
            $this->session->set_flashdata('flash_message', get_phrase('settings_updated'));
            
            redirect(base_url() . 'manage_language', 'refresh');
        }
        $page_data['page_name']        = 'manage_language';
        $page_data['page_title']       = get_phrase('manage_language');
        $page_data['role']=$this->session->userdata('role_id');
        //$page_data['language_phrases'] = $this->db->get('language')->result_array();
        $this->load->view('backend/index', $page_data); 
    }
    
    /*****BACKUP / RESTORE / DELETE DATA PAGE**********/
    function backup_restore($operation = '', $type = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');
        
        if ($operation == 'create') {
            $this->crud_model->create_backup($type);
        }
        if ($operation == 'restore') {
            $this->crud_model->restore_backup();
            $this->session->set_flashdata('backup_message', 'Backup Restored');
            redirect(base_url() . 'backup_restore', 'refresh');
        }
        if ($operation == 'delete') {
            $this->crud_model->truncate($type);
            $this->session->set_flashdata('backup_message', 'Data removed');
            redirect(base_url() . 'backup_restore', 'refresh');
        }
        
        $page_data['page_info']  = 'Create backup / restore from backup';
        $page_data['page_name']  = 'backup_restore';
        $page_data['page_title'] = get_phrase('manage_backup_restore');
        $this->load->view('backend/index', $page_data);
    }
    
    /******MANAGE OWN PROFILE AND CHANGE PASSWORD***/
    function manage_profile($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url() . 'index.php?login', 'refresh');
        if ($param1 == 'update_profile_info') {
            $data['name']  = $this->input->post('name');
            $data['email'] = $this->input->post('email');
            
            $this->db->where('user_id', $this->session->userdata('user_id'));
            $this->db->update('users', $data);
            move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/users_image/' . $this->session->userdata('admin_id') . '.jpg');
            $this->session->set_flashdata('flash_message', get_phrase('account_updated'));
            redirect(base_url() . 'manage_profile', 'refresh');
        }
        if ($param1 == 'update_company_info') {
            $data['distributor_name']  = $this->input->post('distributor_name');
            $data['email'] = $this->input->post('email');
            
            $this->db->where('distributor_id', $this->session->userdata('distributor_id'));
            $this->db->update('distributor', $data);
            move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/distributor_image/' . $this->session->userdata('distributor_id') . '.jpg');
            $this->session->set_flashdata('flash_message', get_phrase('account_updated'));
            redirect(base_url() . 'manage_profile', 'refresh');
        }
        if ($param1 == 'update_server_info') {
            $data['server_ip']  = $this->input->post('server_ip');
            $data['server_port'] = $this->input->post('server_port');
            
            $this->db->where('distributor_id', $this->session->userdata('distributor_id'));
            $this->db->update('server_settings', $data);
            $this->session->set_flashdata('flash_message', get_phrase('account_updated'));
            redirect(base_url() . 'manage_profile', 'refresh');
        }
        if ($param1 == 'change_password') {
            $data['password']             = $this->input->post('password');
            $data['new_password']         = $this->input->post('new_password');
            $data['confirm_new_password'] = $this->input->post('confirm_new_password');
            
            $current_password = $this->db->get_where('users', array(
                'user_id' => $this->session->userdata('admin_id')
            ))->row()->password;
           
            if ((verifyHashedPassword($data['password'], $current_password)) && $data['new_password'] == $data['confirm_new_password']) {
                $this->db->where('user_id', $this->session->userdata('admin_id'));
                $this->db->update('users', array(
                    'password' =>    getHashedPassword($data['new_password'])
                ));
             
        

                $this->session->set_flashdata('flash_message', get_phrase('password_updated'));
            } else {
                $this->session->set_flashdata('flash_message', get_phrase('password_mismatch'));
            }
            redirect(base_url() . 'manage_profile', 'refresh');
        }
        $page_data['page_name']  = 'manage_profile';
        $page_data['role']=$this->session->userdata('role_id');
        $page_data['page_title'] = get_phrase('manage_profile');
        $page_data['edit_data']  = $this->db->get_where('users', array(
            'user_id' => $this->session->userdata('admin_id')
        ))->result_array();
        $page_data['distributor_data']  = $this->db->get_where('distributor', array(
            'distributor_id' => $this->session->userdata('distributor_id')
        ))->result_array();
        $this->db->limit(1);
        $page_data['server_data']  = $this->db->get_where('server_settings', array(
            'distributor_id' => $this->session->userdata('distributor_id')
        ))->result_array();
        $this->load->view('backend/index', $page_data);
    }

    function homepage($param1=""){
        if($param1=="download"){
        
            $current_timestamp = strtotime("now");
            $data['created']= $current_timestamp;
            $this->db->insert('downloads',$data);
        redirect(base_url() . 'homepage');

        }
    $this->frontView('frontend/homepage', $page_data);
    }

    function faq($param1=""){
        if($param1=="download"){
        
            $current_timestamp = strtotime("now");
            $data['created']= $current_timestamp;
            $this->db->insert('downloads',$data);
        redirect(base_url() . 'faq');

        }
    $this->frontView('frontend/faq', $page_data);
    }

    function contact($param1=""){
        if($param1=="download"){
        
            $current_timestamp = strtotime("now");
            $data['created']= $current_timestamp;
            $this->db->insert('downloads',$data);
        redirect(base_url() . 'contact');

        }
    $this->frontView('frontend/contact', $page_data);
    }

    function equity($param1=""){
        if($param1=="download"){
        
            $current_timestamp = strtotime("now");
            $data['created']= $current_timestamp;
            $this->db->insert('downloads',$data);
        redirect(base_url() . 'equity');

        }
    $this->frontView('frontend/equity', $page_data);
    }

function commodities($param1=""){
        if($param1=="download"){
        
            $current_timestamp = strtotime("now");
            $data['created']= $current_timestamp;
            $this->db->insert('downloads',$data);
        redirect(base_url() . 'commodities');

        }
    $this->frontView('frontend/commodities', $page_data);
    }

    function news($param1=""){
        if($param1=="download"){
        
            $current_timestamp = strtotime("now");
            $data['created']= $current_timestamp;
            $this->db->insert('downloads',$data);
        redirect(base_url() . 'news');

        }
    $this->frontView('frontend/news', $page_data);
    }

    function products($param1=""){
        if($param1=="download"){
        
            $current_timestamp = strtotime("now");
            $data['created']= $current_timestamp;
            $this->db->insert('downloads',$data);
        redirect(base_url() . 'products');

        }
    $this->frontView('frontend/products', $page_data);
    }
    function funds($param1=""){
        if($param1=="download"){
        
            $current_timestamp = strtotime("now");
            $data['created']= $current_timestamp;
            $this->db->insert('downloads',$data);
        redirect(base_url() . 'funds');

        }
    $this->frontView('frontend/funds', $page_data);
    }

    function service($param1=""){
        if($param1=="download"){
        
            $current_timestamp = strtotime("now");
            $data['created']= $current_timestamp;
            $this->db->insert('downloads',$data);
        redirect(base_url() . 'service');

        }
    $this->frontView('frontend/service', $page_data);
    }
    
    function portfolio($param1=""){
        if($param1=="download"){
        
            $current_timestamp = strtotime("now");
            $data['created']= $current_timestamp;
            $this->db->insert('downloads',$data);
        redirect(base_url() . 'portfolio');

        }
    $this->frontView('frontend/portfolio', $page_data);
    }

    function icons($param1=""){
        if($param1=="download"){
        
            $current_timestamp = strtotime("now");
            $data['created']= $current_timestamp;
            $this->db->insert('downloads',$data);
        redirect(base_url() . 'icons');

        }
    $this->frontView('frontend/icons', $page_data);
    }
    function typography($param1=""){
        if($param1=="download"){
        
            $current_timestamp = strtotime("now");
            $data['created']= $current_timestamp;
            $this->db->insert('downloads',$data);
        redirect(base_url() . 'typhography');

        }
    $this->frontView('frontend/typography', $page_data);
    }
    function about($param1=""){
        if($param1=="download"){
        
            $current_timestamp = strtotime("now");
            $data['created']= $current_timestamp;
            $this->db->insert('downloads',$data);
        redirect(base_url() . 'about');

        }
    $this->frontView('frontend/about', $page_data);
    }


    function ipo($param1=""){
        if($param1=="download"){
        
            $current_timestamp = strtotime("now");
            $data['created']= $current_timestamp;
            $this->db->insert('downloads',$data);
        redirect(base_url() . 'ipo');

        }
    $this->frontView('frontend/ipo', $page_data);
    }
    function sitemap($param1=""){
        if($param1=="download"){
        
            $current_timestamp = strtotime("now");
            $data['created']= $current_timestamp;
            $this->db->insert('downloads',$data);
        redirect(base_url() . 'sitemap');

        }
    $this->frontView('frontend/sitemap', $page_data);
    }
    /**
     * The following function updates the devices table
     */
    function device_connect($encrypt="", $imei=""){
        $this->load->model('m_data_captured');
       
            try {
               
                $this->m_data_captured->device_authenticate($encrypt, $imei);
            } catch (Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(), "\n"; 
            }   
    }
     /**
     * The following function updates the devices table
     */
    function check_connectivity($encrypt="", $imei=""){
        $this->load->model('m_data_captured');
       
            try {
               
                $this->m_data_captured->check_connectivity($encrypt, $imei);
            } catch (Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(), "\n"; 
            }   
    }


 /**
     * The following function updates the devices table
     */
    function get_familylist($date=''){
        $this->load->model('m_data_captured');
       
            try {
               
               echo $this->m_data_captured->get_familylist($date);
            } catch (Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(), "\n"; 
            }   
    }
    
/**
     * The following function updates the devices table
     */
    function get_learnerlist($date=''){
        $this->load->model('m_data_captured');
       
            try {
               
               echo $this->m_data_captured->get_learnerlist($date);
            } catch (Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(), "\n"; 
            }   
    }
    
function register_magcell(){
    
}

    //function to encrypt passwords
    function password_setup(){
        $password= md5(rand(10,200000));
        return $password;
    }
    //function to compare passwords
    function compare_password(){
        if (verifyHashedPassword('password', '$2y$10$/NMtmepPdwGAKfhTG.5Uker.osBQUI3OgVsnmp92dZpIfp.Q3wCWq')) {
            echo "yes";
        }
        else{
            echo "no";
        };
    }
    //function that returns company logo
    function imageRequest($distributor_id=''){
        if (($distributor_id)==null){
            echo base_url()."uploads/distributor_image/logo.png";
        }else{
            echo base_url()."uploads/distributor_image/".$distributor_id.".jpg"; 
        }
    }
}

