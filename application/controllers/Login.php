<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 	
 * 	@author : Gaudencio Solivatore
 * 	30th July, 2018
 * 	Creative Item
 * 	www.magtouch.co.za
 * 	
 */

class Login extends CI_Controller {
var $password="";
    function __construct() {
        parent::__construct();
        $this->load->model('crud_model');
        $this->load->database();
        $this->load->library('session');
        /* cache control */
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 26 Jul 2010 05:00:00 GMT");
    }

    //Default function, redirects to logged in user area
    public function index() {

        if ($this->session->userdata('admin_login') == 1)
            redirect(base_url() . 'dashboard', 'refresh');

        $this->load->view('backend/login');
    }

    //Ajax login function 
    function ajax_login() {
        $response = array();

        //Recieving post input of email, password from ajax request
        $email = $_POST["email"];
        $this->password=$password = trim($_POST["password"]);
        $response['submitted_data'] = $_POST;

        //Validating login
        $login_status = $this->validate_login($email, $password);
        $response['login_status'] = $login_status;
        if ($login_status == 'success') {
            $response['redirect_url'] = '';
        }

        //Replying ajax request with validation response
        echo json_encode($response);
    }

    //Validating login from ajax request
    function validate_login($email = '', $password = '') {
        $credential = array('email' => $email);


        // Checking login credential for admin
        $query = $this->db->get_where('users', $credential);
        if ($query->num_rows() > 0) {
            $row = $query->row();
            
            if(verifyHashedPassword($password, $row->password)){
                $this->session->set_userdata('admin_login', '1');
                $this->session->set_userdata('admin_id', $row->user_id);
                $this->session->set_userdata('login_user_id', $row->user_id);
                $this->session->set_userdata('name', $row->name);
                $this->session->set_userdata('password', $row->password);
                $this->session->set_userdata('mypassword', $password);
                $this->session->set_userdata('role_id',  $row->level);
                $this->session->set_userdata('distributor_id',  $row->distributor_id);
                $this->session->set_userdata('login_type', 'admin');
                $data['online_status']=1;
                $this->db->where('email', $email);
                $this->db->where('user_id', $row->user_id);
                $this->db->update('users', $data);
               
                return 'success';  
            }
            return 'invalid';
        }

        return 'invalid';
    }

    /*     * *DEFAULT NOR FOUND PAGE**** */

    function four_zero_four() {
        $this->load->view('four_zero_four');
    }

    // PASSWORD RESET BY EMAIL
    function forgot_password($message='')
    {
        $this->session->set_flashdata('flash_message' , get_phrase('data_added_successfully'));
        $this->load->view('backend/forgot_password',$data);
    }
// ACCOUNT ACTIVATION
function account_activate($message='')
{
    global $themessage;
    $data['themessage']=$message;
    $this->load->view('backend/account_activate',$data);
}

    function ajax_forgot_password()
    {
        $resp                   = array();
        $resp['status']         = 'false';
        $email                  = $_POST["email"];
        $reset_account_type     = '';
        //resetting user password here
        $new_password           =   getHashedPassword("admin");

        // Checking credential for admin
        $query = $this->db->get_where('users' , array('email' => $email));
        if ($query->num_rows() > 0) 
        {
            $reset_account_type     =   'admin';
            $this->db->where('email' , $email);
            $this->db->update('users' , array('password' => $new_password));
            $resp['status']         = 'true';
        }
        
       // send new password to user email  
        $this->email_model->password_reset_email($new_password , $reset_account_type , $email);

        $resp['submitted_data'] = $_POST;

        echo json_encode($resp);
    }

    function ajax_account_activate()
    { 
        header("Location:gaulo.com");
        $resp                   = array();
        $resp['status']         = 'false';
        $email                  = $_POST["password"];
        $reset_account_type     = '';
        //resetting user password here
        $new_password           =   getHashedPassword("admin");

        // Checking credential for admin
        $query = $this->db->get_where('users' , array('email' => $email));
        if ($query->num_rows() > 0) 
        {
            $reset_account_type     =   'admin';
            $this->db->where('email' , $email);
            $this->db->update('users' , array('password' => $new_password));
            $resp['status']         = 'true';
        }
        
       // send new password to user email  
        $this->email_model->password_reset_email($new_password , $reset_account_type , $email);

        $resp['submitted_data'] = $_POST;

        echo json_encode($resp);
    }


    /*     * *****LOGOUT FUNCTION ****** */

    function logout() {
        $data['online_status']=0;
        $user_id=$this->session->userdata('admin_id');
        $this->db->where('user_id', $user_id);
        $this->db->update('users', $data);
        $this->session->sess_destroy();
        $this->session->set_flashdata('logout_notification', 'logged_out');
        redirect(base_url().'homepage', 'refresh');
    }

}
