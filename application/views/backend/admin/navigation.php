<div class="sidebar-menu">
    <header class="logo-env" >

        <!-- logo -->
        <div class="logo" style="">
            <a href="<?php echo base_url(); ?>">
            <?php if($role==ROLE_ADMIN){ ?>
                <img src="uploads/logo.png"  style="max-width:90%;max-height:60px;"/>
            <?php }else{ ?>
                <img style="max-width:90%;max-height:60px;" src="<?php 
                 $distributor_id=$this->session->userdata('distributor_id');
                echo $this->crud_model->get_image_url('distributor' ,  $distributor_id);?>" alt="...">
            <?php } ?>                   
            </a>
        </div>

        <!-- logo collapse icon -->
        <div class="sidebar-collapse" style="">
            <a href="#" class="sidebar-collapse-icon with-animation">

                <i class="entypo-menu"></i>
            </a>
        </div>

        <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
        <div class="sidebar-mobile-menu visible-xs">
            <a href="#" class="with-animation">
                <i class="entypo-menu"></i>
            </a>
        </div>
    </header>

    <div style=""></div>	
    <ul id="main-menu" class="">
        <!-- add class "multiple-expanded" to allow multiple submenus to open -->
        <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->


        <!-- DASHBOARD -->
        <li class="<?php if ($page_name == 'dashboard') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>dashboard">
                <i class="entypo-gauge"></i>
                <span><?php echo get_phrase('dashboard'); ?></span>
            </a>
        </li>

        <!-- STUDENT -->
        <li class="<?php
        if ($page_name == 'student_add' ||
                $page_name == 'student_bulk_add' ||
                $page_name == 'student_information' ||
                $page_name == 'student_marksheet')
            echo 'opened active has-sub';
        ?> ">
            <a href="#">
                <i class="entypo-chart-bar"></i>
                <span><?php echo "Reports";//get_phrase('student'); ?></span>
            </a>
            <ul>
                <!-- STUDENT ADMISSION -->
                <li class="<?php if ($page_name == 'student_add') echo 'active'; ?> ">
                    <a href="">
                        <span><i class="entypo-dot"></i> <?php echo "Monthly device report"; ?></span>
                    </a>
                </li>

                <!-- STUDENT BULK ADMISSION -->
                <li class="<?php if ($page_name == 'student_bulk_add') echo 'active'; ?> ">
                    <a href="">
                        <span><i class="entypo-dot"></i> <?php echo "User activity report"; ?></span>
                    </a>
                </li>

                <!-- STUDENT INFORMATION -->
                <li class="<?php if ($page_name == 'student_information' || $page_name == 'student_marksheet') echo 'opened active'; ?> ">
                    <a href="#">
                        <span><i class="entypo-dot"></i> <?php echo "Distributors  report"; ?></span>
                    </a>
                    
                </li>

            </ul>
        </li>
        <?php 
        if($role==ROLE_ADMIN){
            ?>
        <!-- DISTRIBUTORSs -->
        <li class="<?php if ($page_name == 'distributor') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>distributor">
                <i class="entypo-users"></i>
                <span><?php echo "Distributors";//get_phrase('teacher'); ?></span>
            </a>
        </li>
        <?php } ?>

        <?php 
        if($role==ROLE_FAMILY){
            ?>
        <!-- FAMILY LIST -->
        <!--
        <li class="<?php //if ($page_name == 'familylist') echo 'active'; ?> ">
            <a href="<?php //echo base_url(); ?>familylist">
                <i class="entypo-users"></i>
                <span><?php echo "FamilyList";//get_phrase('teacher'); ?></span>
            </a>
        </li>
        -->
         <!-- LEARNER LIST -->
         <li class="<?php if ($page_name == 'learnerlist') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>learnerlist">
                <i class="entypo-users"></i>
                <span><?php echo "LearnerList";//get_phrase('teacher'); ?></span>
            </a>
        </li>
        <!-- PARENT LIST -->
        <li class="<?php if ($page_name == 'parentlist') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>parentlist">
                <i class="entypo-users"></i>
                <span><?php echo "ParentList";//get_phrase('teacher'); ?></span>
            </a>
        </li>
        
        <?php } ?>

 <?php 
        if($role==ROLE_ADMIN || $role==ROLE_DISTRIBUTOR){
            ?>
        <!-- USERS -->
        <li class="<?php
        if ($page_name == 'users' ||
                $page_name == 'section')
            echo 'opened active';
        ?> ">
            <a href="#">
                <i class="entypo-user"></i>
                <span><?php echo "Users";//get_phrase('class'); ?></span>
            </a>
            <ul>
                <li class="<?php if ($page_name == 'class') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>users">
                        <span><i class="entypo-dot"></i> <?php echo "Manage users";//get_phrase('manage_classes'); ?></span>
                    </a>
                </li>
               
            </ul>
        </li>
<!-- DEVICES -->
    <li class="<?php
        if ($page_name == 'devices' ||
                $page_name == 'section')
            echo 'opened active';
        ?> ">
            <a href="#">
                <i class="entypo-monitor"></i>
                <span><?php echo "Devices";//get_phrase('class'); ?></span>
            </a>
            <ul>
                <li class="<?php if ($page_name == 'class') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>devices">
                        <span><i class="entypo-dot"></i> <?php echo "Manage devices";//get_phrase('manage_classes'); ?></span>
                    </a>
                </li>
               
            </ul>
        </li>

   <!-- ALL EMAILS SENT -->
   <li class="<?php
        if ($page_name == 'all_emails_sent' ||
                $page_name == 'section')
            echo 'opened active';
        ?> ">
            <a href="#">
                <i class="entypo-monitor"></i>
                <span><?php echo "Emails sent";//get_phrase('class'); ?></span>
            </a>
            <ul>
                <li class="<?php if ($page_name == 'class') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>all_emails_sent">
                        <span><i class="entypo-dot"></i> <?php echo "View Emails";//get_phrase('manage_classes'); ?></span>
                    </a>
                </li>
               
            </ul>
        </li>

   
        <?php } ?>
       
        <!-- NOTICEBOARD -->
        <?php 
        if($role==ROLE_ADMIN){
            ?>
        <li class="<?php if ($page_name == 'noticeboard') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>noticeboard">
                <i class="entypo-doc-text-inv"></i>
                <span><?php echo get_phrase('noticeboard'); ?></span>
            </a>
        </li>
        <?php } ?>
        <!-- MESSAGE -->
        <?php 
        if($role==ROLE_ADMIN && $role==ROLE_DISTRIBUTOR){
            ?>
        <li class="<?php if ($page_name == 'message') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>message">
                <i class="entypo-mail"></i>
                <span><?php echo get_phrase('message'); ?></span>
            </a>
        </li>
        <?php } ?>
        <!-- SETTINGS -->
        <?php
        if($role==ROLE_ADMIN){
            ?>
        <li class="<?php
        if ($page_name == 'system_settings' ||
                $page_name == 'manage_language' ||
                $page_name == 'server_settings' ||
                    $page_name == 'sms_settings')
                        echo 'opened active';
        ?> ">
            <a href="#">
                <i class="entypo-lifebuoy"></i>
                <span><?php echo get_phrase('settings'); ?></span>
            </a>
            <ul>
                <li class="<?php if ($page_name == 'system_settings') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>system_settings">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('general_settings'); ?></span>
                    </a>
                </li>
        
                <li class="<?php if ($page_name == 'server_settings') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>server_settings">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('server settings'); ?></span>
                    </a>
                </li>
       
                
                <li class="<?php if ($page_name == 'manage_language') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>manage_language">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('language_settings'); ?></span>
                    </a>
                </li>
      
            </ul>
        </li>
        <?php } ?>
        <!-- ACCOUNT -->
        <li class="<?php if ($page_name == 'manage_profile') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>manage_profile">
                <i class="entypo-lock"></i>
                <span><?php echo get_phrase('account'); ?></span>
            </a>
        </li>

    </ul>

</div>