<?php 
$edit_data		=	$this->db->get_where('LearnerList' , array('learner_id' => $param2) )->result_array();
foreach ( $edit_data as $row):
?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" data-collapsed="0">
        	<div class="panel-heading">
            	<div class="panel-title" >
            		<i class="entypo-plus-circled"></i>
					<?php echo get_phrase('Edit LearnerList');?>
            	</div>
            </div>
			<div class="panel-body">
                    <?php echo form_open(base_url() . 'learnerlist/do_update/'.$row['learner_id'] , array('class' => 'form-horizontal form-groups-bordered validate','target'=>'_top', 'enctype' => 'multipart/form-data'));?>
                        		
                                <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('photo');?></label>
                                
                                <div class="col-sm-5">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 100px; height: 100px;" data-trigger="fileinput">
                                            <img src="<?php echo $this->crud_model->get_image_url('family' , $row['learner_id']);?>" alt="...">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                                        <div>
                                            <span class="btn btn-white btn-file">
                                                <span class="fileinput-new">Select image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" name="userfile" accept="image/*">
                                            </span>
                                            <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('name');?></label>
                                <div class="col-sm-5">
                                
                                    <input type="text" class="form-control" name="learner_name" value="<?php echo $row['learner_name'];?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('surname');?></label>
                                <div class="col-sm-5">
                                
                                    <input type="text" class="form-control" name="learner_surname" value="<?php echo $row['learner_surname'];?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo "phone pumber";//get_phrase('birthday');?></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="parent_cellphone" value="<?php echo $row['parent_cellphone'];?>"/>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('learner email');?></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="learner_email" value="<?php echo $row['learner_email'];?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('parent email');?></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="parent_email" value="<?php echo $row['parent_email'];?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('family code');?></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="family_code" maxlength="4" value="<?php echo $row['family_code'];?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('Member pincode');?></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="internal_id" maxlength="6" value="<?php echo $row['internal_id'];?>"/>
                                </div>
                            </div>
                            
                            
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-5">
                                <button type="submit" class="btn btn-info"><?php echo "Update";//get_phrase('edit_teacher');?></button>
                            </div>
                        </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>

<?php
endforeach;
?>