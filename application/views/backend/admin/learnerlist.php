<?php 
    print_r($header);
    print_r($values);
 ?>
<a href="javascript:;" onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/modal_learnerlist_add/');" 
            	class="btn btn-primary pull-right">
                <i class="entypo-plus-circled"></i>
            	<?php echo "Add Learner or Member";//get_phrase('add_new_teacher');?>
                </a> 
                <br><br>

                <form enctype="multipart/form-data" method="post" action="<?php echo base_url() . 'learnerlist/upload'; ?>" >
                <table>
                <tr>
                        <td>
                        <div class="form-group">
                        <input type="file" class="form-control" name="userfile" placeholder"bulk" title="bulk upload">
                        </div>
                       </td>
                       
                        <td> <div class="form-group">
                           
                                <button type="submit" class="btn btn-info"><?php echo "Upload";//get_phrase('edit_teacher');?></button>
                           
                        </div></td>
                        </tr>
                </table>
                </form>
               <table class="table table-bordered datatable" id="table_export">
                    <thead>
                    
                        <tr>
					
                            <th width="80"><div><?php echo get_phrase('photo');?></div></th>
                            <th><div><?php echo get_phrase('name');?></div></th>
                            <th><div><?php echo get_phrase('email');?></div></th>
							<th><div><?php echo get_phrase('phone');?></div></th>
                            <th><div><?php echo "family code";//get_phrase('family code');?></div></th>
                            <th><div><?php echo "member code";//get_phrase('member code');?></div></th>
                            <th><div><?php echo get_phrase('options');?></div></th>
                        </tr>
                    </thead>
                    <tbody>
						<?php 
								$this->db->where('isDeleted',0);
                                $learnerlist	=	$this->db->get('LearnerList' )->result_array();
                                foreach($learnerlist as $row):?>
                        <tr>
                            <td><img src="<?php echo $this->crud_model->get_image_url('family',$row['learner_id']);?>" class="img-circle" width="30" /></td>
                            <td><?php echo $row['learner_name'];?></td>
                            <td><?php echo $row['learner_surname'];?></td>
							<td><?php echo $row['parent_cellphone'];?></td>
                            <td><?php echo $row['family_code'];?></td>
                            <td><?php echo $row['internal_id'];?></td>
                            <td>
                                
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">
                                        
                                        <!-- familylist EDITING LINK -->
                                        <li>
                                        	<a href="#" onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/modal_learnerlist_edit/<?php echo $row['learner_id'];?>');">
                                            	<i class="entypo-pencil"></i>
													<?php echo get_phrase('edit');?>
                                               	</a>
                                        				</li>
                                        <li class="divider"></li>
                                        
                                        <!-- familylist DELETION LINK -->
                                        <li>
                                        	<a href="#" onclick="confirm_modal('<?php echo base_url();?>learnerlist/delete/<?php echo $row['learner_id'];?>');">
                                            	<i class="entypo-trash"></i>
													<?php echo get_phrase('delete');?>
                                               	</a>
                                        				</li>
                                    </ul>
                                </div>
                                
                            </td>
                        </tr>
                        <?php endforeach;?>
                       
                    </tbody>

                </table>



<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">

	jQuery(document).ready(function($)
	{
		

		var datatable = $("#table_export").dataTable({
			"sPaginationType": "bootstrap",
			"sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
			"oTableTools": {
				"aButtons": [
					
					{
						"sExtends": "xls",
						"mColumns": [1,2]
					},
					{
						"sExtends": "pdf",
						"mColumns": [1,2]
					},
					{
						"sExtends": "print",
						"fnSetText"	   : "Press 'esc' to return",
						"fnClick": function (nButton, oConfig) {
							datatable.fnSetColumnVis(0, false);
							datatable.fnSetColumnVis(3, false);
							
							this.fnPrint( true, oConfig );
							
							window.print();
							
							$(window).keyup(function(e) {
								  if (e.which == 27) {
									  datatable.fnSetColumnVis(0, true);
									  datatable.fnSetColumnVis(3, true);
								  }
							});
						},
						
					},
				]
			},
			
		});
		
		$(".dataTables_wrapper select").select2({
			minimumResultsForSearch: -1
		});
	});
		
</script>

