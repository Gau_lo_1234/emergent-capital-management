
<!DOCTYPE html>
<html lang="en">
<head>
<title>Emergent Capital a Corporate Business Category Flat Bootstrap Responsive Website Template | Home :: w3layouts</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Trade Market Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
<link class="include" rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.jqplot.css" />
<!-- calender -->
<link type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.simple-dtpicker.css" rel="stylesheet" />
<!-- //calender -->
<!-- different-chart-bar -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/chart.min.css">
<!-- //different-chart-bar -->
<!-- font-awesome icons -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" />
<!-- //font-awesome icons -->
<!-- js -->
<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.marquee.min.js"></script>
<!-- js -->
<!-- pop-up -->
<link href="<?php echo base_url(); ?>assets/homepage/css/popuo-box.css" rel="stylesheet" type="text/css" media="all" />
<!-- //pop-up -->
<!-- left-chart -->
<script src="<?php echo base_url(); ?>assets/js/jquery.flot.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url(); ?>assets/js/jquery.flot.animator.min.js" type="text/javascript"></script>
<!-- //left-chart -->
<link href="//fonts.googleapis.com/css?family=Muli:300,300i,400,400i" rel="stylesheet">
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/move-top.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
</head>

<body>
<!-- header -->
	<div class="header">
		<div class="w3ls_header_top">
			<div class="container">
				<div class="w3l_header_left">
					<ul class="w3layouts_header">
						<li class="w3layouts_header_list">
							<ul>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">Language<span class="caret"></span></a>
									<div class="mega-dropdown-menu">
										<ul class="dropdown-menu w3_dropdown">
											<li><a href="#">English</a></li> 
											<li><a href="#">Hindi</a></li>
										</ul>              
									</div>	
								</li>
								<li>
									<i>|</i>
								</li>
							</ul>
						</li>
						<li class="w3layouts_header_list">
							<a href="<?php echo base_url(); ?>index.php?login">Login To Trade</a><i>|</i>
						</li>
						<li class="w3layouts_header_list">
							<a href="<?php echo base_url(); ?>faq">FAQ</a><i>|</i>
						</li>
						<li class="w3layouts_header_list">
							<a href="<?php echo base_url(); ?>contact">Contact Us</a>
						</li>
					</ul>
				</div>
				<div class="w3l_header_right">
					<h2><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>+(27)63 969 1493</h2>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
		<div class="w3ls_header_middle">
			<div class="container">
				<div class="agileits_logo">
					<h1><a href="<?php echo base_url(); ?>homepage"><span>Emergent</span> Capital<i>Trade anytime anywhere</i></a></h1>
				</div>
				<div class="agileits_search">
					<form action="#" method="post">
						<input name="Search" type="text" placeholder="Search" required="">
						<select id="agileinfo_search" name="agileinfo_search">
							<option value="commodities">Commodities</option>
							<option value="navs">NAVs</option>
							<option value="quotes">Quotes</option>
							<option value="videos">Videos</option>
							<option value="news">News</option>
							<option value="notices">Notices</option>
							<option value="all">All</option>
						</select>
						<input type="submit" value="Search">
					</form>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
<!-- //header -->
<!-- navigation -->
	<div class="trade_navigation">
		<div class="container">
			<nav class="navbar nav_bottom">
			 <!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header nav_2">
				  <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				</div> 
			   <!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
					<nav class="wthree_nav">
						<ul class="nav navbar-nav nav_1">
							<li class="act"><a href="<?php echo base_url(); ?>homepage">Home</a></li>
							<li><a href="<?php echo base_url(); ?>equity">Equity</a></li>
							<li><a href="<?php echo base_url(); ?>commodities">Commodities</a></li>
							<li><a href="<?php echo base_url(); ?>news">News</a></li>
							<li><a href="<?php echo base_url(); ?>products">Products</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Short Codes<span class="caret"></span></a>				
								<div class="dropdown-menu w3ls_vegetables_menu">
									<ul>	
										<li><a href="<?php echo base_url(); ?>icons">Icons</a></li>
										<li><a href="<?php echo base_url(); ?>typography">Typography</a></li>
									</ul>             
								</div>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Research<span class="caret"></span></a>				
								<div class="dropdown-menu w3ls_vegetables_menu">
									<ul>	
										<li><a href="<?php echo base_url(); ?>funds">Fundamental</a></li>
										<li><a href="<?php echo base_url(); ?>funds">Technical</a></li>
										<li><a href="<?php echo base_url(); ?>funds">Mutual Funds</a></li>
									</ul>             
								</div>				
							</li>
							<li><a href="<?php echo base_url(); ?>service">Customer Service</a></li>
							<li><a href="<?php echo base_url(); ?>portfolio">Portfolio</a></li>
						</ul>
					</nav>
				</div>
			</nav>
		</div>
	</div>
<!-- //navigation -->
	