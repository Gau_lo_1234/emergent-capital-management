
<!-- banner-bottom -->
	<div class="banner-bottom">
		<div class="panel panel-default agile_panel">
			<div class="panel-body agile_panel_body">
				<ul class="demo1">
					<li class="news-item">
						<table class="w3_table_trade">
							<tr>
								<td class="w3_agileits_td demo1_w3_table_trade">
									<table class="agileits_w3layouts_table">
										<tr>
											<td style="color:#01A9CE;text-transform:uppercase;">NIFTY 50</td>
										</tr>
										<tr>
											<td>8,638.23<i><span class="caret"></span>-8.35(-0.10%)</i></td>
										</tr>
									</table>
								</td>
								<td class="demo1_w3_table_trade">
									<table class="agileits_w3layouts_table">
										<tr>
											<td style="color:#01A9CE;text-transform:uppercase;">SILVER</td>
										</tr>
										<tr>
											<td>46,343.56<i><span class="caret"></span>-186.00(-0.40%)</i></td>
										</tr>
									</table>
								</td>
								<td class="demo1_w3_table_trade">
									<table class="agileits_w3layouts_table">
										<tr>
											<td style="color:#01A9CE;">Dollar-Rupee</td>
										</tr>
										<tr>
											<td>66.8650<i class="wthree_i"><span class="caret caret1"></span>0.00(0.00%)</i></td>
										</tr>
									</table>
								</td>
								<td class="demo1_w3_table_trade">
									<table class="agileits_w3layouts_table">
										<tr>
											<td style="color:#01A9CE;text-transform:uppercase;">CRUDEOIL</td>
										</tr>
										<tr>
											<td>3,097.00<i><span class="caret"></span>-7.00(-0.23%)</i></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</li>
					<li class="news-item">
						<table class="w3_table_trade">
							<tr>
								<td class="w3_agileits_td demo1_w3_table_trade">
									<table class="agileits_w3layouts_table">
										<tr>
											<td style="color:#01A9CE;text-transform:uppercase;">gold</td>
										</tr>
										<tr>
											<td>31,350.23<i><span class="caret"></span>-117.00(-0.37%)</i></td>
										</tr>
									</table>
								</td>
								<td class="demo1_w3_table_trade">
									<table class="agileits_w3layouts_table">
										<tr>
											<td style="color:#01A9CE;text-transform:uppercase;">ftsc</td>
										</tr>
										<tr>
											<td>6.887.93<i><span class="caret"></span>-5.99(-0.09%)</i></td>
										</tr>
									</table>
								</td>
								<td class="demo1_w3_table_trade">
									<table class="agileits_w3layouts_table">
										<tr>
											<td style="color:#01A9CE;text-transform:uppercase;">cac</td>
										</tr>
										<tr>
											<td>4,436.70<i><span class="caret"></span>-23.00(0.54%)</i></td>
										</tr>
									</table>
								</td>
								<td class="demo1_w3_table_trade">
									<table class="agileits_w3layouts_table">
										<tr>
											<td style="color:#01A9CE;text-transform:uppercase;">dax</td>
										</tr>
										<tr>
											<td>10,596.00<i><span class="caret"></span>-79.89(-0.75%)</i></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</li>
					<li class="news-item">
						<table class="w3_table_trade">
							<tr>
								<td class="w3_agileits_td demo1_w3_table_trade">
									<table class="agileits_w3layouts_table">
										<tr>
											<td style="color:#01A9CE;text-transform:uppercase;">sensex</td>
										</tr>
										<tr>
											<td>2,7993.64<i><span class="caret"></span>-66.59(-0.24%)</i></td>
										</tr>
									</table>
								</td>
								<td class="demo1_w3_table_trade">
									<table class="agileits_w3layouts_table">
										<tr>
											<td style="color:#01A9CE;text-transform:uppercase;">nhai</td>
										</tr>
										<tr>
											<td>1,256.00<i style="color:#00AA00"><span class="caret caret1"></span>16.20(1.31%)</i></td>
										</tr>
									</table>
								</td>
								<td class="demo1_w3_table_trade">
									<table class="agileits_w3layouts_table">
										<tr>
											<td style="color:#01A9CE;">Euro-Rupee</td>
										</tr>
										<tr>
											<td>75.42<i class="wthree_i"><span class="caret caret1"></span>0.03(0.04%)</i></td>
										</tr>
									</table>
								</td>
								<td class="demo1_w3_table_trade">
									<table class="agileits_w3layouts_table">
										<tr>
											<td style="color:#01A9CE;text-transform:uppercase;">iifl</td>
										</tr>
										<tr>
											<td>1,006.51<i><span class="caret"></span>-1.25(-0.12%)</i></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>							
					</li>
				</ul>
			</div>
		<div class="panel-footer"> </div>
		</div>
		<script type="text/javascript">
			$(function () {
				$(".demo1").bootstrapNews({
					newsPerPage: 1,
					autoplay: true,
					pauseOnHover:true,
					direction: 'up',
					newsTickerInterval: 3000,
					onToDo: function () {
						//console.log(this);
					}
				});
				
				$(".demo2").bootstrapNews({
					newsPerPage: 3,
					autoplay: true,
					pauseOnHover: true,
					navigation: false,
					direction: 'up',
					newsTickerInterval: 2500,
					onToDo: function () {
						//console.log(this);
					}
				});
			});
		</script>
		<script src="<?php echo base_url()."assets/homepage/"; ?>js/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
		<div class='agileinfo_marquee'>
			<div data-speed="10" class="marquee">
				<ul>
					<li><a href="single.html">NPAs of associate banks to weigh on SBI: Religare Capital<span>\</span></a></li>
					<li><a href="single.html">Julius Baer analyst sees opportunities in despised China market</a></li>
				</ul>
			</div>
		</div>
	</div>
<!-- //banner-bottom -->
<!-- news-original -->
	<div class="news-original">
		<div class="container">
			<div class="agileinfo_news_original_grids w3_agile_news_market_grids">
				<div class="col-md-3 agileinfo_portfolio_original_grids_left">
					<div class="portfolio_grid1">
						<a href="single.html"><h3>News</h3></a>
						<p>27 August 2016</p>
						<a href="single.html"><img src="<?php echo base_url()."assets/homepage/"; ?>images/29.jpg" alt=" " class="img-responsive" /></a>
						<div class="portfolio_grid1_anchor">
							<a href="single.html">Top 20 chosen stocks of top 10 fund managers that gave up to 80% returns</a>
						</div>
					</div>
					<div class="portfolio_grid1">
						<a href="single.html"><h3>News</h3></a>
						<p>28 August 2016</p>
						<a href="single.html"><img src="<?php echo base_url()."assets/homepage/"; ?>images/30.jpg" alt=" " class="img-responsive" /></a>
						<div class="portfolio_grid1_anchor">
							<a href="single.html">Power Ministry plans to set up funds for stressed assets</a>
						</div>
					</div>
					<div class="portfolio_grid1">
						<a href="single.html"><h3>News</h3></a>
						<p>28 August 2016</p>
						<a href="single.html"><img src="<?php echo base_url()."assets/homepage/"; ?>images/18.jpg" alt=" " class="img-responsive" /></a>
						<div class="portfolio_grid1_anchor">
							<a href="single.html">Rs 10L crore in 60 days! Look which stocks made the most of the bull run</a>
						</div>
					</div>
					<div class="portfolio_grid1">
						<a href="single.html"><h3>News</h3></a>
						<p>29 August 2016</p>
						<a href="single.html"><img src="<?php echo base_url()."assets/homepage/"; ?>images/31.jpg" alt=" " class="img-responsive" /></a>
						<div class="portfolio_grid1_anchor">
							<a href="single.html">Protests outside Chinese embassy in London over Balochistan: How China is involved in the row</a>
						</div>
					</div>
					<div class="portfolio_grid1">
						<a href="single.html"><h3>News</h3></a>
						<p>31 August 2016</p>
						<a href="single.html"><img src="<?php echo base_url()."assets/homepage/"; ?>images/44.jpg" alt=" " class="img-responsive" /></a>
						<div class="portfolio_grid1_anchor">
							<a href="single.html">Finally, the BSE Sensex ended with a gain of 120 points at 27,902. The BSE Sensex opened at 27,827 touched an intra-day high of 27,953 and low of 27,699</a>
						</div>
					</div>
				</div>
				<div class="col-md-6 agileinfo_portfolio_original_grids_left1">
					<div class="col-md-6 w3_agile_portfolio_grid1_left">
						<div class="portfolio_grid1">
							<a href="single.html"><h3>News</h3></a>
							<p>30 August 2016</p>
							<a href="single.html"><img src="<?php echo base_url()."assets/homepage/"; ?>images/43.jpg" alt=" " class="img-responsive" /></a>
							<div class="portfolio_grid1_anchor">
								<a href="single.html">Shares in Asia mixed as Tokyo jumps on yen, Shanghai down slightly</a>
							</div>
						</div>
						<div class="portfolio_grid1">
							<a href="single.html"><h3>News</h3></a>
							<p>30 August 2016</p>
							<a href="single.html"><img src="<?php echo base_url()."assets/homepage/"; ?>images/28.jpg" alt=" " class="img-responsive" /></a>
							<div class="portfolio_grid1_anchor">
								<a href="single.html">Hiving off one of units of Brazilian subsidiary: Shree Renuka</a>
							</div>
						</div>
						<div class="portfolio_grid1">
							<a href="single.html"><h3>News</h3></a>
							<p>31 August 2016</p>
							<a href="single.html"><img src="<?php echo base_url()."assets/homepage/"; ?>images/25.jpg" alt=" " class="img-responsive" /></a>
							<div class="portfolio_grid1_anchor">
								<a href="single.html">Expect to close FY17 with revenues of over Rs 1000cr: Sequent</a>
							</div>
						</div>
						<div class="portfolio_grid1">
							<a href="single.html"><h3>News</h3></a>
							<p>02 September 2016</p>
							<a href="single.html"><img src="<?php echo base_url()."assets/homepage/"; ?>images/45.jpg" alt=" " class="img-responsive" /></a>
							<div class="portfolio_grid1_anchor">
								<a href="single.html">Real Estate and Construction sector is expected to generate 75 million jobs by 2022: KPMG</a>
							</div>
						</div>
						<div class="portfolio_grid1">
							<a href="single.html"><h3>News</h3></a>
							<p>03 September 2016</p>
							<a href="single.html"><img src="<?php echo base_url()."assets/homepage/"; ?>images/46.jpg" alt=" " class="img-responsive" /></a>
							<div class="portfolio_grid1_anchor">
								<a href="single.html">European shares edge lower; Alstom rises on contract win</a>
							</div>
						</div>
					</div>
					<div class="col-md-6 w3_agile_portfolio_grid1_left">
						<div class="portfolio_grid1">
							<a href="single.html"><h3>News</h3></a>
							<p>03 September 2016</p>
							<a href="single.html"><img src="<?php echo base_url()."assets/homepage/"; ?>images/46.jpg" alt=" " class="img-responsive" /></a>
							<div class="portfolio_grid1_anchor">
								<a href="single.html">European shares edge lower; Alstom rises on contract win</a>
							</div>
						</div>
						<div class="portfolio_grid1">
							<a href="single.html"><h3>News</h3></a>
							<p>03 September 2016</p>
							<a href="single.html"><img src="<?php echo base_url()."assets/homepage/"; ?>images/31.jpg" alt=" " class="img-responsive" /></a>
							<div class="portfolio_grid1_anchor">
								<a href="single.html">Protests outside Chinese embassy in London over Balochistan: How China is involved in the row</a>
							</div>
						</div>
						<div class="portfolio_grid1">
							<a href="single.html"><h3>News</h3></a>
							<p>04 September 2016</p>
							<a href="single.html"><img src="<?php echo base_url()."assets/homepage/"; ?>images/47.jpg" alt=" " class="img-responsive" /></a>
							<div class="portfolio_grid1_anchor">
								<a href="single.html">Dollar at 3-week high, bonds and stocks sell off on hawkish Fed</a>
							</div>
						</div>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="col-md-3 agileinfo_news_original_grids_right agileinfo_portfolio_original_grids_right">
					<div class="w3layouts_add_market">
						<img src="<?php echo base_url()."assets/homepage/"; ?>images/13.jpg" alt=" " class="img-responsive" />
						<div class="w3layouts_add_market_pos">
							<h3>pay demat dues online</h3>
						</div>
					</div>
					<div class="w3_stocks">
						<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
							<ul id="myTab2" class="nav nav-tabs nav-tabs1" role="tablist">
								<li role="presentation" class="active"><a href="#home2" id="home2-tab" role="tab" data-toggle="tab" aria-controls="home2" aria-expanded="true">Stocks</a></li>
								<li role="presentation"><a href="#latest2" role="tab" id="latest2-tab" data-toggle="tab" aria-controls="latest2">Funds</a></li>
								<li role="presentation"><a href="#experts2" role="tab" id="experts2-tab" data-toggle="tab" aria-controls="experts2">Commodities</a></li>
							</ul>
							<div id="myTabContent2" class="tab-content">
								<div role="tabpanel" class="tab-pane fade in active" id="home2" aria-labelledby="home2-tab">
									<div class="w3l_stocks">
										<div class="w3l_stocks1">
											<a href="single.html"><h4>Axis Bank</h4></a>
											<p>579.80<i style="color:#00AA00;"><span class="caret caret1"></span>1.85<label>(0.33%)</label></i></p>
										</div>
										<div class="w3l_stocks1">
											<a href="single.html"><h4>Infosys</h4></a>
											<p>1,016.35<i><span class="caret"></span>-4.95<label>(-0.48%)</label></i></p>
										</div>
										<div class="w3l_stocks1">
											<a href="single.html"><h4>SBI</h4></a>
											<p>255.70<i><span class="caret"></span>-3.75<label>(-1.45%)</label></i></p>
										</div>
										<div class="w3l_stocks1">
											<a href="single.html"><h4>Sun Pharma</h4></a>
											<p>808.25<i><span class="caret"></span>-13.75<label>(-1.75%)</label></i></p>
										</div>
										<div class="w3l_stocks1">
											<a href="single.html"><h4>TCS</h4></a>
											<p>2,548.70<i><span class="caret"></span>-54.80<label>(-2.10%)</label></i></p>
										</div>
									</div>
								</div>
								<div role="tabpanel" class="tab-pane fade" id="latest2" aria-labelledby="latest2-tab">
									<div class="w3l_stocks">
										<div class="w3l_stocks1">
											<a href="single.html"><h4>Kotak Select Focus Fund - Regular (G)</h4></a>
											<p>25.975<i style="color:#00AA00;"><span class="caret caret1"></span>0.06<label>(0.25%)</label></i></p>
										</div>
										<div class="w3l_stocks1">
											<a href="single.html"><h4>Birla SL Frontline Equity (G)</h4></a>
											<p>180.83<i style="color:#00AA00;"><span class="caret caret1"></span>0.14<label>(0.08%)</label></i></p>
										</div>
										<div class="w3l_stocks1">
											<a href="single.html"><h4>SBI Blue Chip Fund (G)</h4></a>
											<p>31.918<i style="color:#00AA00;"><span class="caret caret1"></span>0.02<label>(0.05%)</label></i></p>
										</div>
										<div class="w3l_stocks1">
											<a href="single.html"><h4>Principal Emerging Bluechip (G)</h4></a>
											<p>80.11<i style="color:#00AA00;"><span class="caret caret1"></span>0.37<label>(0.46%)</label></i></p>
										</div>
										<div class="w3l_stocks1">
											<a href="single.html"><h4>SBI Magnum Multicap Funds (G)</h4></a>
											<p>37.592<i style="color:#00AA00;"><span class="caret caret1"></span>0.14<label>(0.36%)</label></i></p>
										</div>
									</div>
								</div>
								<div role="tabpanel" class="tab-pane fade" id="experts2" aria-labelledby="experts2-tab">
									<div class="w3l_stocks">
										<div class="w3l_stocks1">
											<a href="single.html"><h4 style="text-transform:capitalize;">Copper Spt 25,2016</h4></a>
											<p>318.10<i><span class="caret"></span>-4.05<label>(-1.26%)</label></i></p>
										</div>
										<div class="w3l_stocks1">
											<a href="single.html"><h4 style="text-transform:capitalize;">natural gas Spt 24,2016</h4></a>
											<p>177.10<i style="color:#00AA00;"><span class="caret caret1"></span>2.1<label>(1.2%)</label></i></p>
										</div>
										<div class="w3l_stocks1">
											<a href="single.html"><h4 style="text-transform:capitalize;">crude oil Spt 23,2016</h4></a>
											<p>3231.00<i><span class="caret"></span>-61<label>(-1.85%)</label></i></p>
										</div>
										<div class="w3l_stocks1">
											<a href="single.html"><h4 style="text-transform:capitalize;">Silver Spt 22,2016</h4></a>
											<p>44726.00<i><span class="caret"></span>-718<label>(-1.58%)</label></i></p>
										</div>
										<div class="w3l_stocks1">
											<a href="single.html"><h4 style="text-transform:capitalize;">gold Spt 21,2016</h4></a>
											<p>31285.00<i><span class="caret"></span>-119<label>(-0.38%)</label></i></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="agileinfo_chat">
						<h3><i class="fa fa-comments-o" aria-hidden="true"></i>Chat</h3>
						<div class="agileinfo_chat_left">
							<img src="<?php echo base_url()."assets/homepage/"; ?>images/17.jpg" alt=" " class="img-responsive" />
						</div>
						<div class="agileinfo_chat_right">
							<h4>Mark Allen</h4>
							<p>investments in fixed income</p>
						</div>
						<div class="clearfix"> </div>
						<h5><i>Head funds & fixed income research, CRISIL <span>(25 Sep 2016 - 20:00hrs)</span></i></h5>
						<a href="single.html"><span class="blink_me">Chat Now</span></a>
					</div>
					<div class="wthree_international">
						<img src="<?php echo base_url()."assets/homepage/"; ?>images/18.jpg" alt=" " class="img-responsive" />
						<div class="wthree_international_pos">
							<p>international markets</p>
						</div>
					</div>
					<div class="w3layouts_newsletter">
						<h3><i class="fa fa-envelope" aria-hidden="true"></i>Newsletter</h3>
						<form action="#" method="post">
							<input class="email" name="Email" type="email" placeholder="Email" required="">
							<input type="submit" value="Send">
						</form>
						<p>Trade market offers you a choice of email alerts on your investments for FREE!</p>
					</div>
					<div class="w3_latest_stock">
						<img src="<?php echo base_url()."assets/homepage/"; ?>images/19.jpg" alt=" " class="img-responsive" />
						<div class="w3_latest_stock_pos">
							<h3>latest <span>stock <i>market</i> updates</span></h3>
						</div>
					</div>
					<div class="w3l_your_stocks">
						<h3><i class="fa fa-stack-exchange" aria-hidden="true"></i>Your Stocks</h3>
						<form action="#" method="post">
							<span>
								<label>Name</label>
								<input type="text" name="Name" placeholder=" " required="">
							</span>
							<span>
								<label>Mobile</label>
								<input type="text" name="Mobile" placeholder=" " required="">
							</span>
							<span>
								<label>Email</label>
								<input type="email" name="Email" placeholder=" " required="">
							</span>
							<span>
								<label>Location</label>
								<input type="text" name="Location" placeholder=" " required="">
							</span>
							<span>
								<label>Pin</label>
								<input type="text" name="Pin" placeholder=" " required="">
							</span>
							<input type="submit" value="Submit Now">
						</form>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
<!-- //news-original -->

