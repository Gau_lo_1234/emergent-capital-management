
<!-- banner-bottom -->
	<div class="banner-bottom">
		<div class="panel panel-default agile_panel">
			<div class="panel-body agile_panel_body">
				<ul class="demo1">
					<li class="news-item">
						<table class="w3_table_trade">
							<tr>
								<td class="w3_agileits_td demo1_w3_table_trade">
									<table class="agileits_w3layouts_table">
										<tr>
											<td style="color:#01A9CE;text-transform:uppercase;">NIFTY 50</td>
										</tr>
										<tr>
											<td>8,638.23<i><span class="caret"></span>-8.35(-0.10%)</i></td>
										</tr>
									</table>
								</td>
								<td class="demo1_w3_table_trade">
									<table class="agileits_w3layouts_table">
										<tr>
											<td style="color:#01A9CE;text-transform:uppercase;">SILVER</td>
										</tr>
										<tr>
											<td>46,343.56<i><span class="caret"></span>-186.00(-0.40%)</i></td>
										</tr>
									</table>
								</td>
								<td class="demo1_w3_table_trade">
									<table class="agileits_w3layouts_table">
										<tr>
											<td style="color:#01A9CE;">Dollar-Rupee</td>
										</tr>
										<tr>
											<td>66.8650<i class="wthree_i"><span class="caret caret1"></span>0.00(0.00%)</i></td>
										</tr>
									</table>
								</td>
								<td class="demo1_w3_table_trade">
									<table class="agileits_w3layouts_table">
										<tr>
											<td style="color:#01A9CE;text-transform:uppercase;">CRUDEOIL</td>
										</tr>
										<tr>
											<td>3,097.00<i><span class="caret"></span>-7.00(-0.23%)</i></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</li>
					<li class="news-item">
						<table class="w3_table_trade">
							<tr>
								<td class="w3_agileits_td demo1_w3_table_trade">
									<table class="agileits_w3layouts_table">
										<tr>
											<td style="color:#01A9CE;text-transform:uppercase;">gold</td>
										</tr>
										<tr>
											<td>31,350.23<i><span class="caret"></span>-117.00(-0.37%)</i></td>
										</tr>
									</table>
								</td>
								<td class="demo1_w3_table_trade">
									<table class="agileits_w3layouts_table">
										<tr>
											<td style="color:#01A9CE;text-transform:uppercase;">ftsc</td>
										</tr>
										<tr>
											<td>6.887.93<i><span class="caret"></span>-5.99(-0.09%)</i></td>
										</tr>
									</table>
								</td>
								<td class="demo1_w3_table_trade">
									<table class="agileits_w3layouts_table">
										<tr>
											<td style="color:#01A9CE;text-transform:uppercase;">cac</td>
										</tr>
										<tr>
											<td>4,436.70<i><span class="caret"></span>-23.00(0.54%)</i></td>
										</tr>
									</table>
								</td>
								<td class="demo1_w3_table_trade">
									<table class="agileits_w3layouts_table">
										<tr>
											<td style="color:#01A9CE;text-transform:uppercase;">dax</td>
										</tr>
										<tr>
											<td>10,596.00<i><span class="caret"></span>-79.89(-0.75%)</i></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</li>
					<li class="news-item">
						<table class="w3_table_trade">
							<tr>
								<td class="w3_agileits_td demo1_w3_table_trade">
									<table class="agileits_w3layouts_table">
										<tr>
											<td style="color:#01A9CE;text-transform:uppercase;">sensex</td>
										</tr>
										<tr>
											<td>2,7993.64<i><span class="caret"></span>-66.59(-0.24%)</i></td>
										</tr>
									</table>
								</td>
								<td class="demo1_w3_table_trade">
									<table class="agileits_w3layouts_table">
										<tr>
											<td style="color:#01A9CE;text-transform:uppercase;">nhai</td>
										</tr>
										<tr>
											<td>1,256.00<i style="color:#00AA00"><span class="caret caret1"></span>16.20(1.31%)</i></td>
										</tr>
									</table>
								</td>
								<td class="demo1_w3_table_trade">
									<table class="agileits_w3layouts_table">
										<tr>
											<td style="color:#01A9CE;">Euro-Rupee</td>
										</tr>
										<tr>
											<td>75.42<i class="wthree_i"><span class="caret caret1"></span>0.03(0.04%)</i></td>
										</tr>
									</table>
								</td>
								<td class="demo1_w3_table_trade">
									<table class="agileits_w3layouts_table">
										<tr>
											<td style="color:#01A9CE;text-transform:uppercase;">iifl</td>
										</tr>
										<tr>
											<td>1,006.51<i><span class="caret"></span>-1.25(-0.12%)</i></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>							
					</li>
				</ul>
			</div>
		<div class="panel-footer"> </div>
		</div>
		<script type="text/javascript">
			$(function () {
				$(".demo1").bootstrapNews({
					newsPerPage: 1,
					autoplay: true,
					pauseOnHover:true,
					direction: 'up',
					newsTickerInterval: 3000,
					onToDo: function () {
						//console.log(this);
					}
				});
				
				$(".demo2").bootstrapNews({
					newsPerPage: 3,
					autoplay: true,
					pauseOnHover: true,
					navigation: false,
					direction: 'up',
					newsTickerInterval: 2500,
					onToDo: function () {
						//console.log(this);
					}
				});
			});
		</script>
		<script src="js/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
		<div class='agileinfo_marquee'>
			<div data-speed="10" class="marquee">
				<ul>
					<li><a href="single.html">NPAs of associate banks to weigh on SBI: Religare Capital<span>\</span></a></li>
					<li><a href="single.html">Julius Baer analyst sees opportunities in despised China market</a></li>
				</ul>
			</div>
		</div>
	</div>
<!-- //banner-bottom -->
<!-- contact -->
	<div class="agileits_w3layouts_map">
		<iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d27902988.79518621!2d-114.20771246353291!3d31.383073606863928!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1smarket+trade+in+usa!5e0!3m2!1sen!2sin!4v1474518460719" style="border:0"></iframe>
	</div>
	<div class="w3_agileits_keep_touch">
		<div class="container">
			<div class="col-md-4 w3_agileits_keep_touch_left">
				<div class="wthree_keep_touch">
					<i class="fa fa-phone" aria-hidden="true"></i>
				</div>
				<p>(+000) 123 456 345</p>
			</div>
			<div class="col-md-4 w3_agileits_keep_touch_left">
				<div class="wthree_keep_touch">
					<i class="fa fa-home" aria-hidden="true"></i>
				</div>
				<p>3432 Main Street, ASE Lorance, <span>2 New York City, USA</span></p>
			</div>
			<div class="col-md-4 w3_agileits_keep_touch_left">
				<div class="wthree_keep_touch">
					<i class="fa fa-envelope" aria-hidden="true"></i>
				</div>
				<a href="mailto:info@trademarket.com">info@trademarket.com</a>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<div class="contact">
		<div class="container">
			<h3>Contact Us</h3>
			<div class="agile_contact_grids">
				<form action="#" method="post">
					<div class="col-md-6 wthree_contact_left_grid">
						<span class="input input--kuro">
							<input class="input__field input__field--kuro" type="text" id="input-7" placeholder="" required="">
							<label class="input__label input__label--kuro" for="input-7">
								<span class="input__label-content input__label-content--kuro">Name</span>
							</label>
						</span>
						<span class="input input--kuro">
							<input class="input__field input__field--kuro" type="email" id="input-8" placeholder="" required="">
							<label class="input__label input__label--kuro" for="input-8">
								<span class="input__label-content input__label-content--kuro">Email</span>
							</label>
						</span>
						<span class="input input--kuro">
							<input class="input__field input__field--kuro" type="text" id="input-9" placeholder="" required="">
							<label class="input__label input__label--kuro" for="input-9">
								<span class="input__label-content input__label-content--kuro">Subject</span>
							</label>
						</span>
					</div>
					<div class="col-md-6 wthree_contact_left_grid">
						<textarea name="Message" placeholder="Message..." required=""></textarea>
					</div>
					<div class="clearfix"> </div>
					<input type="submit" value="Submit">
				</form>
			</div>
		</div>
	</div>
<!-- //contact -->
<!-- footer -->
	<div class="footer">
		<div class="container">
			<ul class="agileits_w3layouts_footer_info">
				<li><a href="index.html">Home</a><i>|</i></li>
				<li><a href="news.html">Markets</a><i>|</i></li>
				<li><a href="funds.html">mutual funds</a><i>|</i></li>
				<li><a href="commodities.html">commodities</a><i>|</i></li>
				<li><a href="portfolio.html">portfolio</a><i>|</i></li>
				<li><a href="about.html">About Us</a><i>|</i></li>
				<li><a href="ipo.html">IPO</a><i>|</i></li>
				<li><a href="sitemap.html">sitemap</a></li>
			</ul>
			<p>© 2016 Trade Market. All rights reserved | Design by <a href="http://w3layouts.com/">W3layouts</a></p>
		</div>
	</div>

