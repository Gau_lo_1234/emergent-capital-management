<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Email_model extends CI_Model {
	
	function __construct()
    {
        parent::__construct();
    }


	function device_register_email($username='' ,$image='', $email=''){
		$email_sub='Device Registration success';
		$email_msg=' <div style="padding:10px; display:block; background:#2a9fff; width:60%; color:white;">'.$username.' your device settings has been registered and scan qrcode below to get activated</div> <br>'.$image.'';
		$this->load->library('email');
        $this->email->initialize($config);
        $this->email->from('android@onlineguarding.co.za', 'Gaudencio Solivatore');
        $this->email->to($email);
        $this->email->cc('gautest@magtouch.co.za');
        $this->email->bcc('gautest@magtouch.co.za');   
		$this->email->subject($email_sub);
		$this->email->message($email_msg);
    //$this->email->attach($myqrimage.$myfile);

		$page['from']   = "gautest@magtouch.co.za";
		$page['to']=$email;
		$page['body']=$email_msg;
		$page['title']=$email_sub;
		$this->db->insert('email_sent', $page);

      $this->email->send();
        
       
      
        //echo $this->email->print_debugger();
       
	   //changed few files
	}

	function account_opening_email($account_type = '' , $email = '', $password_set='')
	{
		$system_name	=	$this->db->get_where('settings' , array('type' => 'system_name'))->row()->description;
		
		$email_msg		=	"Welcome to ".$system_name."<br />";
		$email_msg		.=	"Your account type : ".$account_type."<br />";
		$email_msg		.=	"Account activate here : <a href='".base_url()."index.php?login/account_activate/".$password_set."'>click here</a><br />";
		
		$email_sub		=	"Account opening email";
		$email_to		=	$email;
        $data['from'] =$from  = "gautest@magtouch.co.za";
		$data['to']=$email_to;
		$data['body']=$email_msg;
		$data['title']=$email_sub;
		$this->db->insert('email_sent', $data);
		
		$this->do_email($email_msg , $email_sub , $email_to, $from);

		
	}
	
	function password_reset_email($email = '',$password_set='')
	{
		$query=	$this->db->get_where('users' , array('email' => $email));
		if($query->num_rows() > 0)
		{
			
			$email_msg		.=	"Setup password : <a href='".base_url()."index.php?login/account_activate/".$password_set."'>click here</a><br />";
		
			
			$email_sub	=	"Password setup request";
			$email_to	=	$email;
			$data['from']=$from  = "gautest@magtouch.co.za";
			$data['to']=$email;
			$data['body']=$email_msg;
			$data['title']=$email_sub;
			$page['password_set']=$password_set;
			$this->db->insert('email_sent', $data);
			$this->db->where('email',$email);
			$this->db->update('users', $page);
			$email_to="gautest@magtouch.co.za";
			
			try {
				$this->do_email($email_msg , $email_sub , $email_to, $from);
				$this->session->set_flashdata('password_set', 'success');
				header("Location:".base_url()."index.php?login");
			}catch(Exception $ex){
			};
		
			return true;
		}
		else
		{	
			$this->session->set_flashdata('password_set', 'failed');	
			header("Location:".base_url()."index.php?login");
			return false;
		}
	}
	

	function account_activate($password='', $password_set='')
	{
		$query=	$this->db->get_where('users' , array('password_set' => $password_set));
		if($query->num_rows() > 0)
		{
			
			$email_msg		.=	"password has been reset successfully";
		
			
			$email_sub	=	"Password reset success";
			$email_to	=	$email;
			$data['from'] =$from= "gautest@magtouch.co.za";
			$data['to']=$email;
			$data['body']=$email_msg;
			$data['title']=$email_sub;
			$page['password']= getHashedPassword(trim($password));
			$this->db->where('password_set',$password_set);
			$this->db->update('users', $page);
			$email_to="gautest@magtouch.co.za";
			$this->do_email($email_msg , $email_sub , $email_to, $from);
			$this->session->set_flashdata('password_set', 'success');
				header("Location:".base_url()."index.php?login");
			return true;
		}
		else
		{	
			$this->session->set_flashdata('password_set', 'failed');	
			header("Location:".base_url()."index.php?login");
			return false;
		}
	}
	

	/***custom email sender****/
	function do_email($msg=NULL, $sub=NULL, $to=NULL, $from=NULL)
	{
		if($from=NULL){
			$from="gautest@magtouch.co.za";
		}
		$msg	=	$msg."<br /><br /><br /><br /><br /><br /><br /><hr /><center><a href=\"http://liveguarding.co.za/fav2/\">&copy; 2018 Android Device Management Prp</a></center>";
		
		$this->load->library('email');
        $this->email->initialize($config);
        $this->email->from('android@onlineguarding.co.za', 'Gaudencio Solivatore');
        $this->email->to($to);
        $this->email->cc('gautest@magtouch.co.za');
        $this->email->bcc('gautest@magtouch.co.za');   
		$this->email->subject($sub);
		$this->email->message($msg);
    //$this->email->attach($myqrimage.$myfile);

		
		//$msg	=	$msg."<br /><br /><br /><br /><br /><br /><br /><hr /><center><a href=\"http://liveguarding.co.za/fav2/\">&copy; 2018 Android Device Management Prp</a></center>";
		//$this->email->message($msg);
		
		$this->email->send();
		
		//echo $this->email->print_debugger();
	}
}

